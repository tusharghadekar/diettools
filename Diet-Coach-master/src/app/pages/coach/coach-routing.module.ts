import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CoachComponent } from './coach.component';
import { CoachListComponent } from './coach-list/coach-list.component';
import { AddCoachComponent } from './add-coach/add-coach.component';

const routes: Routes = [{
  path: '',
  component: CoachComponent,
  children: [{
    path: 'coach-list',
    component: CoachListComponent,
  }, {
    path: 'add-coach',
    component: AddCoachComponent,
  }, 
],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoachRoutingModule { }

export const routedComponents = [
  CoachComponent,
  CoachListComponent,
  AddCoachComponent,
 
];