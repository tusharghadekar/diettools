import { Component} from '@angular/core';

@Component({
  selector: 'app-coach',
  template: `<router-outlet></router-outlet>`,
})
export class CoachComponent{

}
