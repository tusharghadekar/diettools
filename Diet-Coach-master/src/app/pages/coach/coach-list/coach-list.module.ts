import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoachListRoutingModule } from './coach-list-routing.module';

@NgModule({
  imports: [
    CommonModule,
    CoachListRoutingModule
  ],
  declarations: []
})
export class CoachListModule { }
