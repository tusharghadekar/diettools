import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddCoachRoutingModule } from './add-coach-routing.module';

@NgModule({
  imports: [
    CommonModule,
    AddCoachRoutingModule
  ],
  declarations: []
})
export class AddCoachModule { }
