import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThemeModule } from '../../@theme/theme.module';
import { CoachComponent } from './coach.component';
import { CoachRoutingModule } from './coach-routing.module';
import { CoachListComponent } from './coach-list/coach-list.component';
import { AddCoachComponent } from './add-coach/add-coach.component';

@NgModule({
  imports: [
    ThemeModule,
    CommonModule,
    CoachRoutingModule
  ],
  declarations: [CoachComponent,CoachListComponent, AddCoachComponent]
})
export class CoachModule { }
