import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyProfileComponent} from './my-profile.component';

@NgModule({
  imports: [
    CommonModule,
  
  ],
  declarations: [MyProfileComponent]
})
export class MyProfileModule { }
