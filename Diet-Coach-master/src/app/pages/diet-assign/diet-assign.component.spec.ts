import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DietAssignComponent } from './diet-assign.component';

describe('DietAssignComponent', () => {
  let component: DietAssignComponent;
  let fixture: ComponentFixture<DietAssignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DietAssignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DietAssignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
