import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DietAssignComponent } from './diet-assign.component';
//import {NgxPaginationModule} from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    //NgxPaginationModule,
    Ng2SearchPipeModule,
    FormsModule

  ],
  declarations: [DietAssignComponent]
})
export class DietAssignModule { }
