import { Pipe, Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { HttpHeaders } from '@angular/common/http';
import { ModalModule } from "ngx-modal";
import 'rxjs/add/operator/map';
//import { DragDropDirectiveModule} from "angular4-drag-drop";
import { Router, ActivatedRoute, Params } from '@angular/router';
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-diet-assign',
  templateUrl: './diet-assign.component.html',
  styleUrls: ['./diet-assign.component.scss']
})
export class DietAssignComponent implements OnInit {


  server_url: string = environment.devServer_url;
  VegFoodFilter: any = { typeOfFood: 'Vegetarian' };
  NonVegFoodFilter: any = { typeOfFood: 'Non-Vegetarian' };

  userFilter: any;


  public httpOptions = {
    headers: new HttpHeaders({
      'Authorization': 'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViY2VkNDMyMTFkZmE0MDY1ZmI3YjA3MiIsImVtYWlsIjoidGVzdEB0ZXN0LmNvbSIsInJvbGUiOiJ1c2VyIiwiaWF0IjoxNTQwMjgxMzk0LCJleHAiOjE1NDI4NzMzOTR9.LK-n3jBpNTdHz0HUfIG73lr81oqi5PN5P7jX8BnCL6E',
      'Content-Type': 'application/json',

    })
  };
  constructor(private http: HttpClient, private _router: Router, private route: ActivatedRoute) { }

  startDate;
  endDate;
  objectClient;

  getClient() {

    let localUrl = "client/getClients";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.objectClient = res['client'];
          // .filter(
          //   active => active.status === "active");
          // this.UserDataCount = res['count'];
          //this.objectActiveClientCount = this.objectActiveClient.length;
        },

        err => { alert("Error occoured"); }
      );
  }
  clientID;
  getassignClientId(clientID) {
    debugger
    // $("#xyzx").addClass("actives");
    this.clientID = clientID;

  }
  coach_id = '5bced5c38790ef0633af8536';
  public postAssignDietClient(): void {
    // e.preventDefault();
    let dietid = this.route.snapshot.paramMap.get('id');
    debugger

    const postAssignClientObject = {

      clientID: this.clientID,
      dietID: dietid,
      coachID : this.coach_id,
      startDate: this.startDate,
      endDate :this.endDate
    }

    let localUrl = "diet/assign";
    let url = this.server_url + localUrl;
    this.http.post(url, JSON.stringify(postAssignClientObject), this.httpOptions)
      .subscribe(
        (res) => {
          if(res['status']== 'Success') {
           alert("You assigned diet successfully..");
          // //  this.viewProduct ='';
          // // $("#myModal").modal("hide");
          //  this.getDatabookexpense();
          //     }else {
          //       alert("Product NOT Added");
          }
        },
        err => {
          alert("Error occoured");
          // this.viewAuditor ='';
        }
      );

  }

  ngOnInit() {
    this.getClient();
  }

}
