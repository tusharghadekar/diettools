import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import {NgxPaginationModule} from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { CoachClientsComponent} from './coach-clients.component';

@NgModule({
  imports: [
    CommonModule,
    Ng2SearchPipeModule
    
  ],
  declarations: [CoachClientsComponent]
})
export class CoachClientsModule { }
