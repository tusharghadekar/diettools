import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Router, ActivatedRoute, Params } from '@angular/router';
declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'app-coach-clients',
  templateUrl: './coach-clients.component.html',
  styleUrls: ['./coach-clients.component.scss']
})
export class CoachClientsComponent implements OnInit {
  server_url: string = environment.devServer_url;
  
  public httpOptions = {
    headers: new HttpHeaders({
      'Authorization':'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViY2VkNDMyMTFkZmE0MDY1ZmI3YjA3MiIsImVtYWlsIjoidGVzdEB0ZXN0LmNvbSIsInJvbGUiOiJ1c2VyIiwiaWF0IjoxNTQwMjgxMzk0LCJleHAiOjE1NDI4NzMzOTR9.LK-n3jBpNTdHz0HUfIG73lr81oqi5PN5P7jX8BnCL6E',
      'Content-Type': 'application/json',
   
    })
  };
  constructor(private http:HttpClient,private _router: Router) { }

  objectCoachClient=[];

  getCoachClientActive(){
  
    let localUrl = "client/getClients";
    let url = this.server_url + localUrl;

    this.http.get(url,this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.objectCoachClient = res['client'].filter(
            active => active.status === "active");
          // this.UserDataCount = res['count'];
        },
        
        err => { alert("Error occoured");}
      );
   }

   getCoachClientDeactive(){
  
    let localUrl = "client/getClients";
    let url = this.server_url + localUrl;

    this.http.get(url,this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.objectCoachClient = res['client'].filter(
            deactive => deactive.status === "inactive");
          // this.UserDataCount = res['count'];
        },
        
        err => { alert("Error occoured");}
      );
   }

   redirect(id) {
     debugger
    this._router.navigate(['/pages/client-dashboard/'+id]);
  }

  ngOnInit() {
    $(document).ready(function(){
	
      $('ul.tabs li').click(function(){
        var tab_id = $(this).attr('data-tab');
    
        $('ul.tabs li').removeClass('current');
        $('.tab-content').removeClass('current');
    
        $(this).addClass('current');
        $("#"+tab_id).addClass('current');
      });
    
    });

    this.getCoachClientActive();
  }

}
