import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WorkoutPlanComponent} from './workout-plan.component';

@NgModule({
  imports: [
    CommonModule,
   
  ],
  declarations: [WorkoutPlanComponent]
})
export class WorkoutPlanModule { }
