import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { CoachAccountModule } from './coach-account/coach-account.module';
import { CoachClientsModule } from './coach-clients/coach-clients.module';
import { MonitorProgressModule } from './monitor-progress/monitor-progress.module';
import { ClientDashboardModule } from './client-dashboard/client-dashboard.module';

import { PagesRoutingModule } from './pages-routing.module';
import { ThemeModule } from '../@theme/theme.module';
import { MyProfileModule } from './my-profile/my-profile.module';
import { DietToolModule } from './diet-tool/diet-tool.module';
import { WorkoutPlanModule } from './workout-plan/workout-plan.module';
import { ClientReviewModule } from './client-review/client-review.module';
import { DaitPreviewModule } from './dait-preview/dait-preview.module';
import {ModalModule} from "ngx-modal";
import { DietAssignModule } from './diet-assign/diet-assign.module';
import { FormsModule } from '@angular/forms';
import { DietListComponent } from './diet-list/diet-list.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { LoginComponent } from './login/login.component';



const PAGES_COMPONENTS = [
  PagesComponent,
];

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    DashboardModule,
    CoachAccountModule,
    CoachClientsModule,
    MonitorProgressModule,
    ClientDashboardModule,
    MyProfileModule,
    DietToolModule,
    WorkoutPlanModule,
    ClientReviewModule,
    DaitPreviewModule,
    DietAssignModule,
    FormsModule,
    NgxPaginationModule,
    Ng2SearchPipeModule

  ],
  declarations: [
    ...PAGES_COMPONENTS,
    DietListComponent,
    LoginComponent,
  ],
})
export class PagesModule {
}
