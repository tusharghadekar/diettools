import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-push-notification',
  template: `<router-outlet></router-outlet>`,
})
export class PushNotificationComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
