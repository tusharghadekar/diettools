import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForCoachComponent } from './for-coach.component';

describe('ForCoachComponent', () => {
  let component: ForCoachComponent;
  let fixture: ComponentFixture<ForCoachComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForCoachComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForCoachComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
