import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ForCoachRoutingModule } from './for-coach-routing.module';

@NgModule({
  imports: [
    CommonModule,
    ForCoachRoutingModule
  ],
  declarations: []
})
export class ForCoachModule { }
