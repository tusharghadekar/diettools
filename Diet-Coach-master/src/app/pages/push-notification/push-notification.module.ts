import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThemeModule } from '../../@theme/theme.module';
import { PushNotificationComponent } from './push-notification.component';
import { PushNotificationRoutingModule } from './push-notification-routing.module';
import { ForClientComponent } from './for-client/for-client.component';
import { ForCoachComponent } from './for-coach/for-coach.component';

@NgModule({
  imports: [
    ThemeModule,
    CommonModule,
    PushNotificationRoutingModule
  ],
  declarations: [PushNotificationComponent,ForClientComponent, ForCoachComponent]
})
export class PushNotificationModule { }
