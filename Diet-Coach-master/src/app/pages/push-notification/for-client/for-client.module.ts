import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ForClientRoutingModule } from './for-client-routing.module';

@NgModule({
  imports: [
    CommonModule,
    ForClientRoutingModule
  ],
  declarations: []
})
export class ForClientModule { }
