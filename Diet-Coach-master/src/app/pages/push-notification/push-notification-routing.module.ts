import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PushNotificationComponent } from './push-notification.component';
import { ForClientComponent } from './for-client/for-client.component';
import { ForCoachComponent } from './for-coach/for-coach.component';

const routes: Routes = [{
  path: '',
  component: PushNotificationComponent,
  children: [{
    path: 'for-client',
    component: ForClientComponent,
  }, {
    path: 'for-coach',
    component: ForCoachComponent,
  }, 
],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PushNotificationRoutingModule { }

export const routedComponents = [
  PushNotificationComponent,
  ForClientComponent,
  ForCoachComponent,
 
];