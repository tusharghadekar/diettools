import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MonitorProgressComponent } from './monitor-progress.component';

@NgModule({
  imports: [
    CommonModule,
 
  ],
  declarations: [MonitorProgressComponent]
})
export class MonitorProgressModule { }
