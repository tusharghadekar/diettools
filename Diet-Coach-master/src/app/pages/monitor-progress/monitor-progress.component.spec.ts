import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitorProgressComponent } from './monitor-progress.component';

describe('MonitorProgressComponent', () => {
  let component: MonitorProgressComponent;
  let fixture: ComponentFixture<MonitorProgressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonitorProgressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitorProgressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
