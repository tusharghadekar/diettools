import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewClientRoutingModule } from './new-client-routing.module';

@NgModule({
  imports: [
    CommonModule,
    NewClientRoutingModule
  ],
  declarations: []
})
export class NewClientModule { }
