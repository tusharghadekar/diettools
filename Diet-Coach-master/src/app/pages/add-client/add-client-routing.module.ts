import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddClientComponent } from './add-client.component';
import { NewClientComponent } from './new-client/new-client.component';


const routes: Routes = [{
  path: '',
  component: AddClientComponent,
  children: [{
    path: 'new-client',
    component: NewClientComponent,
  }, 
],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddClientRoutingModule { }

export const routedComponents = [
  AddClientComponent,
  NewClientComponent,

];
