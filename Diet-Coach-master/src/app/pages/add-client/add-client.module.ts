import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThemeModule } from '../../@theme/theme.module';
import { AddClientRoutingModule } from './add-client-routing.module';
import {AddClientComponent } from './add-client.component';
import { NewClientComponent } from './new-client/new-client.component';

@NgModule({
  imports: [
    ThemeModule,
    CommonModule,
    AddClientRoutingModule
  ],
  declarations: [ AddClientComponent,NewClientComponent]
})

export class AddClientModule { }
