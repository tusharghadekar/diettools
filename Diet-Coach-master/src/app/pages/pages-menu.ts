import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'nb-home',
    link: '/pages/dashboard',
    home: true,
  },

  // {
  //   title: 'My Profile',
  //   icon: 'nb-home',
  //   link: '/pages/my-profile',
  //   home: true,
  // },

  // {
  //   title: 'Account',
  //   icon: 'nb-home',
  //   link: '/pages/coach-account',
  //   home: true,
  // },

  {
    title: 'Clients',
    icon: 'nb-home',
    link: '/pages/coach-clients',
    home: true,
  },

  {
    title: 'Diet Tools',
    icon: 'nb-home',
    link: '/pages/diet-tool',
    home: true,
  },


  // {
  //   title: 'Progress Monitor',
  //   icon: 'nb-home',
  //   link: '/pages/monitor-progress',
  //   home: true,
  // }, 

  /* {
    title: 'Client Review',
    icon: 'nb-home',
    link: '/pages/client-review',
    home: true,
  }, 

  {
    title: 'Account Info',
    icon: 'nb-title',
    link: '/pages/account',
    children: [
      {
        title: 'Total Amount',
        link: '/pages/account/total-amount',
      },
      {
        title: 'All Payments Details',
        link: '/pages/account/all-payment-details',
      },
      {
        title: 'Add Cash Payments',
        link: '/pages/account/add-cash-payments',
      },
      {
        title: 'Refund Payments',
        link: '/pages/account/refund-payments',
      },
      
    ],
  },


  {
    title: 'Clients',
    icon: 'nb-title',
    link: '/pages/clients',
    children: [
      {
        title: 'Daily New Clients',
        link: '/pages/clients/daily-new-clients',
      },
      {
        title: 'Active Clients',
        link: '/pages/clients/active-clients',
      },
      {
        title: 'Archived Clients',
        link: '/pages/clients/archived-clients',
      },
      {
        title: 'Active Clients Progress',
        link: '/pages/clients/active-clients-progress',
      },
      {
        title: 'Archieved Clients Progress',
        link: '/pages/clients/archived-clients-progress',
      },
      
    ],
  },
  {
    title: 'Add Client',
    icon: 'nb-keypad',
    link: '/pages/add-client',
    children: [
      {
        title: 'Add New Client',
        link: '/pages/add-client/new-client',
      },
      
    ],
  },
  
  {
    title: 'Coach',
    icon: 'nb-keypad',
    link: '/pages/coach',
    children: [
      {
        title: 'Coach List',
        link: '/pages/coach/coach-list',
      },
      {
        title: 'Add Coach',
        link: '/pages/coach/add-coach',
      },
     
    ],
  },

  {
    title: 'Push Notification',
    icon: 'nb-keypad',
    link: '/pages/push-notification',
    children: [
      {
        title: 'To Clients',
        link: '/pages/push-notification/for-client',
      },

      {
        title: 'To Coach',
        link: '/pages/push-notification/for-coach',
      },
      
    ],
  },


  {
    title: 'FEATURES',
    group: true,
  },
  {
    title: 'UI Features',
    icon: 'nb-keypad',
    link: '/pages/ui-features',
    children: [
      {
        title: 'Buttons',
        link: '/pages/ui-features/buttons',
      },
      {
        title: 'Grid',
        link: '/pages/ui-features/grid',
      },
      {
        title: 'Icons',
        link: '/pages/ui-features/icons',
      },
      {
        title: 'Modals',
        link: '/pages/ui-features/modals',
      },
      {
        title: 'Typography',
        link: '/pages/ui-features/typography',
      },
      {
        title: 'Animated Searches',
        link: '/pages/ui-features/search-fields',
      },
      {
        title: 'Tabs',
        link: '/pages/ui-features/tabs',
      },
    ],
  },
  {
    title: 'Forms',
    icon: 'nb-compose',
    children: [
      {
        title: 'Form Inputs',
        link: '/pages/forms/inputs',
      },
      {
        title: 'Form Layouts',
        link: '/pages/forms/layouts',
      },
    ],
  },
  {
    title: 'Components',
    icon: 'nb-gear',
    children: [
      {
        title: 'Tree',
        link: '/pages/components/tree',
      }, {
        title: 'Notifications',
        link: '/pages/components/notifications',
      },
    ],
  },
  {
    title: 'Maps',
    icon: 'nb-location',
    children: [
      {
        title: 'Google Maps',
        link: '/pages/maps/gmaps',
      },
      {
        title: 'Leaflet Maps',
        link: '/pages/maps/leaflet',
      },
      {
        title: 'Bubble Maps',
        link: '/pages/maps/bubble',
      },
    ],
  },
  {
    title: 'Charts',
    icon: 'nb-bar-chart',
    children: [
      {
        title: 'Echarts',
        link: '/pages/charts/echarts',
      },
      {
        title: 'Charts.js',
        link: '/pages/charts/chartjs',
      },
      {
        title: 'D3',
        link: '/pages/charts/d3',
      },
    ],
  },
  
  {
    title: 'Tables',
    icon: 'nb-tables',
    children: [
      {
        title: 'Smart Table',
        link: '/pages/tables/smart-table',
      },
    ],
  },
      */
  // {
  //   title: 'Auth',
  //   icon: 'nb-locked',
  //   children: [
  //     {
  //       title: 'Login',
  //       link: '/auth/login',
  //     },
  //     {
  //       title: 'Register',
  //       link: '/auth/register',
  //     },
  //     {
  //       title: 'Request Password',
  //       link: '/auth/request-password',
  //     },
  //     {
  //       title: 'Reset Password',
  //       link: '/auth/reset-password',
  //     },
  //   ],
  // },
];
