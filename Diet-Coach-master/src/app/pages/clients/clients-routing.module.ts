import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientsComponent } from './clients.component';
import { ActiveClientsComponent } from './active-clients/active-clients.component';
import { ArchivedClientsComponent } from './archived-clients/archived-clients.component';
import { DailyNewClientsComponent } from './daily-new-clients/daily-new-clients.component';
import { ActiveClientsProgressComponent } from './active-clients-progress/active-clients-progress.component';
import { ArchivedClientsProgressComponent } from './archived-clients-progress/archived-clients-progress.component';

const routes: Routes = [{
  path: '',
  component: ClientsComponent,
  children: [{
    path: 'active-clients',
    component: ActiveClientsComponent,
  }, {
    path: 'archived-clients',
    component: ArchivedClientsComponent,
  }, {
    path: 'daily-new-clients',
    component: DailyNewClientsComponent,
  }, {
    path: 'active-clients-progress',
    component: ActiveClientsProgressComponent,
  }, {
    path: 'archived-clients-progress',
    component: ArchivedClientsProgressComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientsRoutingModule { }


export const routedComponents = [
  ClientsComponent,
  ArchivedClientsComponent,
  DailyNewClientsComponent,
  ActiveClientsProgressComponent,
  ArchivedClientsProgressComponent,
];