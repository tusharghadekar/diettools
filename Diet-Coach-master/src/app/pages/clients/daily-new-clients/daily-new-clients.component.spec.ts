import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyNewClientsComponent } from './daily-new-clients.component';

describe('DailyNewClientsComponent', () => {
  let component: DailyNewClientsComponent;
  let fixture: ComponentFixture<DailyNewClientsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyNewClientsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyNewClientsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
