import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiveClientsProgressComponent } from './active-clients-progress.component';

describe('ActiveClientsProgressComponent', () => {
  let component: ActiveClientsProgressComponent;
  let fixture: ComponentFixture<ActiveClientsProgressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActiveClientsProgressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiveClientsProgressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
