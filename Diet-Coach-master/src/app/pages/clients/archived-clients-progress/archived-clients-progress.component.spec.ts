import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchivedClientsProgressComponent } from './archived-clients-progress.component';

describe('ArchivedClientsProgressComponent', () => {
  let component: ArchivedClientsProgressComponent;
  let fixture: ComponentFixture<ArchivedClientsProgressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArchivedClientsProgressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchivedClientsProgressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
