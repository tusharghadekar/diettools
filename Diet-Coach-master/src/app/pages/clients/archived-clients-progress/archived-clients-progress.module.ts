import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArchivedClientsProgressComponent } from './archived-clients-progress.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ArchivedClientsProgressComponent]
})
export class ArchivedClientsProgressModule { }
