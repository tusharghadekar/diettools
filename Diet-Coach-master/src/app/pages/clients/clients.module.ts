import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThemeModule } from '../../@theme/theme.module';
import { ClientsRoutingModule } from './clients-routing.module';
import { ClientsComponent } from './clients.component';
import { ActiveClientsComponent } from './active-clients/active-clients.component';
import { ArchivedClientsComponent } from './archived-clients/archived-clients.component';
import { DailyNewClientsComponent } from './daily-new-clients/daily-new-clients.component';
import { ActiveClientsProgressComponent } from './active-clients-progress/active-clients-progress.component';
import { ArchivedClientsProgressComponent } from './archived-clients-progress/archived-clients-progress.component';



@NgModule({
  imports: [
    ThemeModule,
    CommonModule,
    ClientsRoutingModule
  ],
  declarations: [
    ClientsComponent,
    ActiveClientsComponent,
    ArchivedClientsComponent,
    DailyNewClientsComponent,
    ActiveClientsProgressComponent,
    ArchivedClientsProgressComponent,
  ]
})
export class ClientsModule { }
