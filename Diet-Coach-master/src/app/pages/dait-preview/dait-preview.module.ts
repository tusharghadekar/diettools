import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import {ModalModule} from "ngx-modal";
import { DaitPreviewComponent } from './dait-preview.component';

@NgModule({
  imports: [
    CommonModule,
    ModalModule
  ],
  declarations: [DaitPreviewComponent]
})
export class DaitPreviewModule { }
