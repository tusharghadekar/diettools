import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DaitPreviewComponent } from './dait-preview.component';

describe('DaitPreviewComponent', () => {
  let component: DaitPreviewComponent;
  let fixture: ComponentFixture<DaitPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DaitPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DaitPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
