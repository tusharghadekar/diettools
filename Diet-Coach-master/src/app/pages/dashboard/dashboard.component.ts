import { Pipe, Component, OnInit,Input  } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { HttpHeaders } from '@angular/common/http';
import {ModalModule} from "ngx-modal";
import 'rxjs/add/operator/map';
//import { DragDropDirectiveModule} from "angular4-drag-drop";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { id } from '../../../../node_modules/@swimlane/ngx-charts/release/utils';
declare var jquery: any;
declare var $: any

@Component({
  selector: 'ngx-dashboard',
  styleUrls: ['./dashboard.component.scss'],
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit{
  server_url: string = environment.devServer_url;

  public httpOptions = {
    headers: new HttpHeaders({
      'Authorization': 'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViY2VkNDMyMTFkZmE0MDY1ZmI3YjA3MiIsImVtYWlsIjoidGVzdEB0ZXN0LmNvbSIsInJvbGUiOiJ1c2VyIiwiaWF0IjoxNTQwMjgxMzk0LCJleHAiOjE1NDI4NzMzOTR9.LK-n3jBpNTdHz0HUfIG73lr81oqi5PN5P7jX8BnCL6E',
      'Content-Type': 'application/json',

    })
  };
  constructor(private http: HttpClient,private _router: Router) { }
coach_id = '5bced5c38790ef0633af8536';

objectGetCoachList=[];
  objectGetCoachListCount;

  objectGetCoachEarning;
  totalAmount:number = 0;
  TotalTDSdeducted:number = 0;
  TotalServiceTax:number = 0;
  TotalNetAmount:number = 0;
  getCoachList(){
  debugger
    let localUrl = "coach/getCoach?coachID="+this.coach_id;
    let url = this.server_url + localUrl;
   
    this.http.get(url,this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.objectGetCoachList = res['coach'];
          // this.UserDataCount = res['count'];
          //this.objectGetCoachListCount = this.objectGetCoachList.length;
          this.objectGetCoachEarning= res['coach']['earnings'];

           for(let i =0;i<this.objectGetCoachEarning.length;i++){
this.totalAmount += Number(this.objectGetCoachEarning[i].TotalAmount);
// .reduce((acc, cur) => acc + cur, 0);
this.TotalTDSdeducted += Number(this.objectGetCoachEarning[i].TDS_Deducted);
// .reduce((acc, cur) => acc + cur, 0);
this.TotalServiceTax += Number(this.objectGetCoachEarning[i].Service_Tax);
this.TotalNetAmount += Number(this.objectGetCoachEarning[i].net_Amount);
}
        },
        
        err => { alert("Error occoured");}
      );
   }
   objectDietListt;
   objectDietList;
   getDietListOverdue(){
    debugger
        //let id = this.route.snapshot.paramMap.get('id');
        this.objectDietList=[];
      
        let localUrl = "diet/getAssignedDiet?coachID="+this.coach_id;
        let url = this.server_url + localUrl;
        var dte:Date = new Date();
        this.http.get(url,this.httpOptions)
          .subscribe(
            (res: Response) => {
              ;
              this.objectDietList = res['diet']['diets'];
              this.objectDietListt = res['diet']['allDiets'].filter(diet =>diet.startDate!=null);
              this.objectDietListt = res['diet']['allDiets'].filter(diet => new Date(diet.endDate)< new Date());
             
                // diet.endDate.setDate(diet.endDate.getDate() - 1)

              this.objectDietListt.forEach(obj => {

                let localUrl = "client/getClients?clientID="+obj.clientID;
                //let localUrl = "brand/";
                let url = this.server_url + localUrl;
                
                this.http.get(url,this.httpOptions)
                  .subscribe(
                    (res: Response) => {
                      var xyz=res['client'];
                      obj.firstName=xyz["firstName"];
                      obj.lastName=xyz["lastName"];
                      // this.spinnerService.hide();
                    },
                    //err => { alert("Error occoured"); this.spinnerService.hide(); }
                  );
                
            });

            },
            
            err => { alert("Error occoured");}
          );
       }

       getDietList(){
        debugger
            //let id = this.route.snapshot.paramMap.get('id');
            this.objectDietList=[];
            // var date = new Date()

            // Add a day
            
            let localUrl = "diet/getAssignedDiet?coachID="+this.coach_id;
            let url = this.server_url + localUrl;
            var dte:Date = new Date();
            this.http.get(url,this.httpOptions)
              .subscribe(
                (res: Response) => {
                  ;
                  this.objectDietList = res['diet']['diets'];
                  this.objectDietListt = res['diet']['allDiets'].filter(diet =>diet.startDate!=null);
                  this.objectDietListt = res['diet']['allDiets'].filter(diet => new Date(diet.endDate)> new Date());
                  // this.objectDietListt = res['diet']['allDiets'].filter( diet => new Date() + 1 == new Date(diet.endDate) ));
                    // diet.endDate.setDate(diet.endDate.getDate() - 1)
    
                  this.objectDietListt.forEach(obj => {
    
                    let localUrl = "client/getClients?clientID="+obj.clientID;
                    //let localUrl = "brand/";
                    let url = this.server_url + localUrl;
                    
                    this.http.get(url,this.httpOptions)
                      .subscribe(
                        (res: Response) => {
                          var xyz=res['client'];
                          obj.firstName=xyz["firstName"];
                          obj.lastName=xyz["lastName"];
                          // this.spinnerService.hide();
                        },
                        //err => { alert("Error occoured"); this.spinnerService.hide(); }
                      );
                    
                });
    
                },
                
                err => { alert("Error occoured");}
              );
           }
ngOnInit() {
 
  this.getCoachList();
  this.getDietList();

  this.objectDietList=[];
  this.objectGetCoachList=[];
  this.objectGetCoachEarning=[];

  $(document).ready(function(){
     
    $('ul.tabs li').click(function(){
      var tab_id = $(this).attr('data-tab');
  
      $('ul.tabs li').removeClass('current');
      $('.tab-content').removeClass('current');
  
      $(this).addClass('current');
      $("#"+tab_id).addClass('current');
    });
  
  })

 
}

}
