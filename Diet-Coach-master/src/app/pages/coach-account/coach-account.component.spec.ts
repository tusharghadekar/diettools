import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoachAccountComponent } from './coach-account.component';

describe('CoachAccountComponent', () => {
  let component: CoachAccountComponent;
  let fixture: ComponentFixture<CoachAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoachAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoachAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
