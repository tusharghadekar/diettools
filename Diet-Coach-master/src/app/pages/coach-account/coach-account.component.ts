import { Component, OnInit } from '@angular/core';
declare var jquery:any;
declare var $ :any;
@Component({
  selector: 'app-coach-account',
  templateUrl: './coach-account.component.html',
  styleUrls: ['./coach-account.component.scss']
})
export class CoachAccountComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    $('.card').on('click', function() {
      if ($(this).hasClass('open')) {
        $('.card').removeClass('open');
        $('.card').removeClass('shadow-2');
        $(this).addClass('shadow-1');
        return false;
      } else {
        $('.card').removeClass('open');
        $('.card').removeClass('shadow-2');
        $(this).addClass('open');
        $(this).addClass('shadow-2');
      }
    });
  }

}
