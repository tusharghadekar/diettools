import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoachAccountComponent } from './coach-account.component';


@NgModule({
  imports: [
    CommonModule,
   
  ],
  declarations: [CoachAccountComponent]
})
export class CoachAccountModule { }
