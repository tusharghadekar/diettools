import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientReviewComponent} from './client-review.component';

@NgModule({
  imports: [
    CommonModule,
   
  ],
  declarations: [ClientReviewComponent]
})
export class ClientReviewModule { }
