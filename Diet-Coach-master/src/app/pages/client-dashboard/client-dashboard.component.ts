import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {RequestOptions} from '@angular/http';
//import { HttpHeaders } from '@angular/common/http'
declare var jquery:any;
declare var $ :any;
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-client-dashboard',
  templateUrl: './client-dashboard.component.html',
  styleUrls: ['./client-dashboard.component.scss']
})
export class ClientDashboardComponent implements OnInit {

  constructor(private http:HttpClient,private _router: Router, private route: ActivatedRoute) { }

  server_url: string = environment.devServer_url;
  
  public httpOptions = {
    headers: new HttpHeaders({
      'Authorization':'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViY2VkNDMyMTFkZmE0MDY1ZmI3YjA3MiIsImVtYWlsIjoidGVzdEB0ZXN0LmNvbSIsInJvbGUiOiJ1c2VyIiwiaWF0IjoxNTQwMjgxMzk0LCJleHAiOjE1NDI4NzMzOTR9.LK-n3jBpNTdHz0HUfIG73lr81oqi5PN5P7jX8BnCL6E',
      'Content-Type': 'application/json',
   
    })
  };

  id;
  hideElement= true;
  objectCoachClientDashDetails;
  // getCoachClientDash(){

  //   //let id = this.route.snapshot.paramMap.get('id');
  
  //   let localUrl = "client/getClients?clientID="+this.ClientID;
  //   let url = this.server_url + localUrl;

  //   this.http.get(url,this.httpOptions)
  //     .subscribe(
  //       (res: Response) => {
  //         this.objectCoachClientDash = res['client']
  //         // this.UserDataCount = res['count'];
  //       },
        
  //       err => { alert("Error occoured");}
  //     );
  //  }
   objectClientAssignedDiet=[];
   objectClientAssignedDietArray=[];
   getAssignedDiet(){
debugger
if(this.ClientID== undefined){
  alert('please select client first');
}else{
    //let id = this.route.snapshot.paramMap.get('id');
    this.objectClientAssignedDiet=[];
  
    let localUrl = "diet/getAssignedDiet?clientID="+this.ClientID;
    let url = this.server_url + localUrl;

    this.http.get(url,this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.objectClientAssignedDiet=[];
          this.objectClientAssignedDiet = res['diet']['allDiets'];
        },
        
        err => { alert("Error occoured");}
      );
   }
  }

   objectCoachClient=[];

  getCoachClientActive(){
  
    let localUrl = "client/getClients";
    let url = this.server_url + localUrl;

    this.http.get(url,this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.objectCoachClient = res['client'].filter(
            active => active.status === "active");
          // this.UserDataCount = res['count'];
        },
        
        err => { alert("Error occoured");}
      );
   }

   getCoachClientDeactive(){
  
    let localUrl = "client/getClients";
    let url = this.server_url + localUrl;

    this.http.get(url,this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.objectCoachClient = res['client'].filter(
            deactive => deactive.status === "inactive");
          // this.UserDataCount = res['count'];
        },
        
        err => { alert("Error occoured");}
      );
   }
ClientID;
   getClientID(id){
    // this.ClientID=[];
    this.hideElement=false;
    this.objectClientAssignedDiet=null;
    //this.objectClientAssignedDietArray=[];
    this.ClientID = id;
debugger
    let localUrl = "client/getClients?clientID="+this.ClientID;
    let url = this.server_url + localUrl;

    this.http.get(url,this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.objectCoachClientDashDetails = res['client'];
          // this.UserDataCount = res['count'];
         
        },
        
        err => { alert("please Select Client");}

      );
      
      //this.getAssignedDiet(this.ClientID);
   }

//    deleteDiet(id) {
//     //console.log("user to delete:" + user._id);

//       let localUrl = "/diet/deleteDiet";
//       let url = this.server_url + localUrl;
   
//    this.http.delete(url,this.httpOptions)
//       .subscribe(
//         (res:Response) => {
// if(res){
//   alert("Deleted Successfully");
// }
//       },);

     
// }

objectClientProgress
getClientProgress(){
  debugger
  if(this.ClientID== undefined){
    alert('please select client first');
  }else{
      //let id = this.route.snapshot.paramMap.get('id');
      this.objectClientProgress=[];
      let localUrl = "client/getBodyIndicators?clientID="+this.ClientID;
      let url = this.server_url + localUrl;
  
      this.http.get(url,this.httpOptions)
        .subscribe(
          (res: Response) => {
            this.objectClientProgress = res['bodyIndicators'];
          },
          
          err => { alert("no progress to display");}
        );
      }
     }


     public postProgressStatus(ProgressStatus): void{
      // e.preventDefault();
    
      debugger
  
      const postPaymentObject = {
        clientID:this.ClientID,
       Progress:ProgressStatus,
        
  
       }
   
   let localUrl = "client/edit";
   let url = this.server_url+localUrl;
   this.http.put(url, JSON.stringify(postPaymentObject),this.httpOptions)
   .subscribe(
      (res) => {
        if(res['status']== 'Success') {
         alert("You added Satus successfully..");
        // //  this.viewProduct ='';
        // // $("#myModal").modal("hide");
        //  this.getDatabookexpense();
        //     }else {
        //       alert("Product NOT Added");
            }
  
        
      },
      err => {alert( "Error occoured");
     
    }
      );
         
  }

//   public postWorkoutFile(plan): void{
//     // e.preventDefault();
  
//     debugger

//     const postPaymentObject = {
//       clientID:this.ClientID,
//      Progress:ProgressStatus,
      

//      }
 
//  let localUrl = "client/edit";
//  let url = this.server_url+localUrl;
//  this.http.put(url, JSON.stringify(postPaymentObject),this.httpOptions)
//  .subscribe(
//     (res) => {
//       if(res['status']== 'Success') {
//        alert("You added Satus successfully..");
//       // //  this.viewProduct ='';
//       // // $("#myModal").modal("hide");
//       //  this.getDatabookexpense();
//       //     }else {
//       //       alert("Product NOT Added");
//           }

      
//     },
//     err => {alert( "Error occoured");
//    // this.viewAuditor ='';
//   }
//     );
       
// }
workoutLevel;
postWorkoutLevel(workoutLevel){
this.workoutLevel=workoutLevel;
}

uploadForm = new FormGroup ({
  //file1: new FormControl()
});
filedata:any;
myFiles:string;
fileChange(e){
  console.log(e);
debugger;
//let Key = e.target.elements[1].value;
//let fileList: FileList = e.target.files;
 this.myFiles = e.target.files[0];
  // for (var i = 0; i < e.target.files.length; i++) { 
    // this.myFiles.push(e.target.files);
  // }
}

postWorkoutFile(e) {
  // let currentUserRoleid = JSON.parse( localStorage.getItem('userRoleid'));
  //   if(currentUserRoleid == "2"){
  //     alert("You Are Not Authenticate to Upload Sales Data")
  //   }else{
  
  e.preventDefault();
  debugger;
  var httpOptions = {
    headers: new HttpHeaders({
      'Authorization':'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViY2VkNDMyMTFkZmE0MDY1ZmI3YjA3MiIsImVtYWlsIjoidGVzdEB0ZXN0LmNvbSIsInJvbGUiOiJ1c2VyIiwiaWF0IjoxNTQwMjgxMzk0LCJleHAiOjE1NDI4NzMzOTR9.LK-n3jBpNTdHz0HUfIG73lr81oqi5PN5P7jX8BnCL6E'
   
    })
  };
  //var headers = new Headers();
  //let formData:FormData = new FormData();
  //let file: File = this.filedata;

  const formData = new FormData();

    // for (var i = 0; i < this.myFiles.length; i++) { 
      formData.append("file", this.myFiles);
    // }

  //let Key = FileList[0];
  //console.log(this.uploadForm);
  formData.append("uploadWorkoutLevel",this.workoutLevel);
   formData.append("clientID",this.ClientID);
  // formData.append("address",this.address_line1);
  // formData.append("city",this.city_name);
  // formData.append("state",this.State_name);
  // formData.append("zipCode",this.zip_code);
  // formData.append("country",this.country_name);
  // formData.append("gender",this.gender_name);
  // formData.append("phone",this.phone_number);
  // formData.append("email",this.email_address);
  // formData.append("bankName",this.Bank_Name);
  // formData.append("bankAccountNumber",this.Bank_account);
  // formData.append("bank_IFSC_Number",this.Bank_IFSC);
  // formData.append("branch",this.Bank_branch);
  //formData.append("_id",id);
//   for (var key of formData.entries()) {
//     console.log(key[0] + ', ' + key[1]);
// }
  
  //formData.append("Key","targetLineFile");
  //let headers = new Headers();
  //headers.append('Content-Type', 'multipart/form-data');
   //headers.append('Authorization', this.tokens); 
  //let Options = new RequestOptions({ headers: headers });
  let localUrl = "client/uploadWorkout";
  let url = this.server_url+localUrl;
  this.http.post(url,formData,httpOptions)
  .subscribe((res)=>{
    console.log(formData);
    if(res['status'] == "Success") {
      alert("You added Workout successfully..");
      
      // this.viewopsVehicleSalesData ='';
      // this.getVehicleSalesMasterData();
       }else {
        alert("File NOT Uploaded");
     
      }
       
  },
  // error => {
  //   if((error['error']['status'] == false) &&  (error['error']['messageCode'] == "QIZ021")) {
  //     alert( "City,Vehicle Versions,Transmission Type and Colour Can Not Be Blank or Wrong"+" "+JSON.stringify(error['error']['data']));
  //   } else if((error['error']['status'] == false) && (error['error']['error'])) {
  //     alert( "vin must be unique."+" "+JSON.stringify(error['error']['messageCode']['fields']));
  // }
  // else{alert( "Failed to Import."); }
  // }
);

// }
}
DueDate;
  ngOnInit() {
    this.getCoachClientActive();
    this.objectClientAssignedDiet=[];
   this.objectClientAssignedDietArray=[];
   this.objectClientProgress=[];
   this.objectCoachClientDashDetails=[];
    $(document).ready(function(){
     
      $('ul.tabs li').click(function(){
        var tab_id = $(this).attr('data-tab');
    
        $('ul.tabs li').removeClass('current');
        $('.tab-content').removeClass('current');
    
        $(this).addClass('current');
        $("#"+tab_id).addClass('current');
      });

      $('ul.tabs_2 li').click(function(){
        var tab_id = $(this).attr('data-tab');
    
        $('ul.tabs_2 li').removeClass('current');
        $('.tab-content_2').removeClass('current');
    
        $(this).addClass('current');
        $("#"+tab_id).addClass('current');
      })
    
    })

  }

  

}
