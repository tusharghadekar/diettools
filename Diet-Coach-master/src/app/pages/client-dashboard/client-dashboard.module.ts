import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FormsModule } from '@angular/forms';
import { ClientDashboardComponent } from './client-dashboard.component';


@NgModule({
  imports: [
    CommonModule,
    Ng2SearchPipeModule,
    FormsModule
  ],
  declarations: [ClientDashboardComponent]
})
export class ClientDashboardModule { }
