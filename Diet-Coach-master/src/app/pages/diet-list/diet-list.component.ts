import { Pipe, Component, OnInit,Input  } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { HttpHeaders } from '@angular/common/http';
import {ModalModule} from "ngx-modal";
import 'rxjs/add/operator/map';
//import { DragDropDirectiveModule} from "angular4-drag-drop";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { id } from '../../../../node_modules/@swimlane/ngx-charts/release/utils';
declare var jquery: any;
declare var $: any

@Component({
  selector: 'app-diet-list',
  templateUrl: './diet-list.component.html',
  styleUrls: ['./diet-list.component.scss']
})
export class DietListComponent implements OnInit {

  server_url: string = environment.devServer_url;

  public httpOptions = {
    headers: new HttpHeaders({
      'Authorization': 'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViY2VkNDMyMTFkZmE0MDY1ZmI3YjA3MiIsImVtYWlsIjoidGVzdEB0ZXN0LmNvbSIsInJvbGUiOiJ1c2VyIiwiaWF0IjoxNTQwMjgxMzk0LCJleHAiOjE1NDI4NzMzOTR9.LK-n3jBpNTdHz0HUfIG73lr81oqi5PN5P7jX8BnCL6E',
      'Content-Type': 'application/json',

    })
  };
  constructor(private http: HttpClient,private _router: Router) { }
coach_id = '5bced5c38790ef0633af8536';

  objectDietList;
  getDietList(){
   debugger
       //let id = this.route.snapshot.paramMap.get('id');
       
     
       let localUrl = "diet/getDiet?coachID="+this.coach_id;
       let url = this.server_url + localUrl;
   
       this.http.get(url,this.httpOptions)
         .subscribe(
           (res: Response) => {
             ;
             this.objectDietList = res['diet'];
           },
           
           err => { alert("Error occoured");}
         );
      }


AssignDiet(dietId){
  window.open(
    '#/pages/diet-assign/'+dietId,
    '_blank' // <- This is what makes it open in a new window.
  );
}


page:number= 1;
 pageChange(e){
  this.page = e;
  console.log(e);


}

  ngOnInit() {
    this.objectDietList=[];
    this.getDietList();
  }

}
