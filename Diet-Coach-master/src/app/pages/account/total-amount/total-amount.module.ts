import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TotalAmountRoutingModule } from './total-amount-routing.module';

@NgModule({
  imports: [
    CommonModule,
    TotalAmountRoutingModule
  ],
  declarations: []
})
export class TotalAmountModule { }
