import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RefundPaymentsRoutingModule } from './refund-payments-routing.module';

@NgModule({
  imports: [
    CommonModule,
    RefundPaymentsRoutingModule
  ],
  declarations: []
})
export class RefundPaymentsModule { }
