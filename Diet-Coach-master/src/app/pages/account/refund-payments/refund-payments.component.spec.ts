import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RefundPaymentsComponent } from './refund-payments.component';

describe('RefundPaymentsComponent', () => {
  let component: RefundPaymentsComponent;
  let fixture: ComponentFixture<RefundPaymentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RefundPaymentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefundPaymentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
