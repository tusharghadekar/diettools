import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThemeModule } from '../../@theme/theme.module';
import { AccountComponent } from './account.component';
import { AccountRoutingModule } from './account-routing.module';
import { TotalAmountComponent } from './total-amount/total-amount.component';
import { AllPaymentDetailsComponent } from './all-payment-details/all-payment-details.component';
import { AddCashPaymentsComponent } from './add-cash-payments/add-cash-payments.component';
import { RefundPaymentsComponent } from './refund-payments/refund-payments.component';

@NgModule({
  imports: [
    ThemeModule,
    CommonModule,
    AccountRoutingModule
  ],
  declarations: [AccountComponent,TotalAmountComponent, AllPaymentDetailsComponent, AddCashPaymentsComponent, RefundPaymentsComponent]
})
export class AccountModule { }
