import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddCashPaymentsRoutingModule } from './add-cash-payments-routing.module';

@NgModule({
  imports: [
    CommonModule,
    AddCashPaymentsRoutingModule
  ],
  declarations: []
})
export class AddCashPaymentsModule { }
