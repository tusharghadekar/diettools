import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountComponent } from './account.component';
import { TotalAmountComponent } from './total-amount/total-amount.component';
import { AllPaymentDetailsComponent } from './all-payment-details/all-payment-details.component';
import { AddCashPaymentsComponent } from './add-cash-payments/add-cash-payments.component';
import { RefundPaymentsComponent } from './refund-payments/refund-payments.component';

const routes: Routes = [{
  path: '',
  component: AccountComponent,
  children: [{
    path: 'total-amount',
    component: TotalAmountComponent,
  }, {
    path: 'all-payment-details',
    component: AllPaymentDetailsComponent,
  }, {
    path: 'add-cash-payments',
    component: AddCashPaymentsComponent,
  }, 
  {
    path: 'refund-payments',
    component: RefundPaymentsComponent,
  }, 
],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule { }

export const routedComponents = [
  AccountComponent,
  TotalAmountComponent,
  AddCashPaymentsComponent,
  RefundPaymentsComponent,
];
