import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllPaymentDetailsComponent } from './all-payment-details.component';

describe('AllPaymentDetailsComponent', () => {
  let component: AllPaymentDetailsComponent;
  let fixture: ComponentFixture<AllPaymentDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllPaymentDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllPaymentDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
