import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AllPaymentDetailsRoutingModule } from './all-payment-details-routing.module';

@NgModule({
  imports: [
    CommonModule,
    AllPaymentDetailsRoutingModule
  ],
  declarations: []
})
export class AllPaymentDetailsModule { }
