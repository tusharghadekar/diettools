import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AccountComponent } from './account/account.component';
import { CoachAccountComponent } from './coach-account/coach-account.component';
import { CoachClientsComponent } from './coach-clients/coach-clients.component';
import { MonitorProgressComponent } from './monitor-progress/monitor-progress.component';
import { ClientDashboardComponent } from './client-dashboard/client-dashboard.component';
import { MyProfileComponent } from './my-profile/my-profile.component';
import { DietToolComponent } from './diet-tool/diet-tool.component';
import { WorkoutPlanComponent } from './workout-plan/workout-plan.component';
import { ClientReviewComponent } from './client-review/client-review.component';
import { DaitPreviewComponent } from './dait-preview/dait-preview.component';
import { DietAssignComponent } from './diet-assign/diet-assign.component';
import { DietListComponent } from './diet-list/diet-list.component';
const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [{
    path: 'dashboard',
    component: DashboardComponent,
  },
  {
    path: 'my-profile',
    component: MyProfileComponent,
  },
  {
    path: 'coach-account',
    component: CoachAccountComponent,
  },

  {
    path: 'coach-clients',
    component: CoachClientsComponent,
  },
  {
    path: 'diet-tool',
    component: DietToolComponent,
  },
  {
    path: 'diet-list',
    component: DietListComponent,
  },
  {
    path: 'diet-assign/:id',
    component: DietAssignComponent,
  },

  {
    path: 'workout-plan',
    component: WorkoutPlanComponent,
  },

  {
    path: 'dait-preview',
    component: DaitPreviewComponent,
  },

  {
    path: 'monitor-progress',
    component: MonitorProgressComponent,
  },

  {
    path: 'client-dashboard',
    component: ClientDashboardComponent,
  },

  {
    path: 'client-review',
    component: ClientReviewComponent,
  },

  {
    path: 'account',
    loadChildren: './account/account.module#AccountModule',
  },
  {
    path: 'add-client',
    loadChildren: './add-client/add-client.module#AddClientModule',
  },
  {
    path: 'coach',
    loadChildren: './coach/coach.module#CoachModule',
  },
  {
    path: 'push-notification',
    loadChildren: './push-notification/push-notification.module#PushNotificationModule',
  },
  {
    path: 'ui-features',
    loadChildren: './ui-features/ui-features.module#UiFeaturesModule',
  }, {
    path: 'components',
    loadChildren: './components/components.module#ComponentsModule',
  }, {
    path: 'maps',
    loadChildren: './maps/maps.module#MapsModule',
  }, {
    path: 'charts',
    loadChildren: './charts/charts.module#ChartsModule',
  }, {
    path: 'clients',
    loadChildren: './clients/clients.module#ClientsModule',
  }, {
    path: 'forms',
    loadChildren: './forms/forms.module#FormsModule',
  }, {
    path: 'tables',
    loadChildren: './tables/tables.module#TablesModule',
  }, {
    path: '',
    redirectTo: '/auth/login',
    pathMatch: 'full',
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
