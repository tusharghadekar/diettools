import { Pipe, Component, OnInit,Input  } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { HttpHeaders } from '@angular/common/http';
import {ModalModule} from "ngx-modal";
import 'rxjs/add/operator/map';
//import { DragDropDirectiveModule} from "angular4-drag-drop";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { id } from '../../../../node_modules/@swimlane/ngx-charts/release/utils';
declare var jquery: any;
declare var $: any;
@Component({
  selector: 'app-diet-tool',
  templateUrl: './diet-tool.component.html',
  styleUrls: ['./diet-tool.component.scss']
})

export class DietToolComponent implements OnInit {
  server_url: string = environment.devServer_url;
  VegFoodFilter: any = { typeOfFood: 'Vegetarian' };
  NonVegFoodFilter: any = { typeOfFood: 'Non-Vegetarian' };

  userFilter: any;

  @Input() //dietPlan: any;

  public httpOptions = {
    headers: new HttpHeaders({
      'Authorization': 'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViY2VkNDMyMTFkZmE0MDY1ZmI3YjA3MiIsImVtYWlsIjoidGVzdEB0ZXN0LmNvbSIsInJvbGUiOiJ1c2VyIiwiaWF0IjoxNTQwMjgxMzk0LCJleHAiOjE1NDI4NzMzOTR9.LK-n3jBpNTdHz0HUfIG73lr81oqi5PN5P7jX8BnCL6E',
      'Content-Type': 'application/json',

    })
  };
  constructor(private http: HttpClient,private _router: Router) { }
  Description;
  Calorie_Range;
  Diet_Preference;
  // coach_id = '5bced5c38790ef0633af8536';
  coach_id = '5bced5c38790ef0633af8536';
  prime_Ingredients;
  header_text;
  footer_text;
  diet_type1;
  diet_type2;
  diet_type3;
  diet_type4;
  objectDietFoodItem = [];

  DuplicatedFood: any = [];


  foodItems = []

  selectDayfirst:boolean;

  getDietFoodItem() {

    let localUrl = "foodItem/getFoodItem";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.objectDietFoodItem = res['foodItem'];
          // this.UserDataCount = res['count'];
        },

        err => { alert("Error occoured"); }
      );
  }
  objectDietFoodItemVeg;
  getFilterDietFoodItem() {

    let localUrl = "foodItem/getFoodItem";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.objectDietFoodItemVeg = res['foodItem'].filter(type => {
            return type.typeOfFood === 'Vegetarian';
          });
          // this.UserDataCount = res['count'];
        },

        err => { alert("Error occoured"); }
      );
  }
  objectDietFoodItemNonVeg
  getFilterDietFoodItem2() {

    let localUrl = "foodItem/getFoodItem";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.objectDietFoodItemNonVeg = res['foodItem'].filter(type => {
            return type.typeOfFood === 'Non-Vegetarian';
          });
          // this.UserDataCount = res['count'];
        },

        err => { alert("Error occoured"); }
      );
  }

  // deleteFat;
  // deleteCarbs;
  // deleteProtien;
  // deleteCalories;
  DeleteValue = (itemmm) => {
    debugger
    console.log(itemmm);

    var array = this.dietPlan['days'][this.day][this.meal].foodItems;
console.log(array)
var index = array.indexOf(itemmm);
if (index > -1) {
  array.splice(index, 1);
}

// var array2 = this.selectedFat;
// console.log(array2)
// var index2 = array2.indexOf(Number(itemmm['fat']));
// if (index2 > -1) {
//   array2.splice(index2, 1);
// }

// var array3 = this.selectedCarbs;
// console.log(array3)
// var index3 = array3.indexOf(Number(itemmm['carbs']));
// if (index3 > -1) {
//   array3.splice(index3, 1);
// }

// var array4 = this.selectedProtine;
// console.log(array4)
// var index4 = array4.indexOf(Number(itemmm['protien']));
// if (index4 > -1) {
//   array4.splice(index4, 1);
// }

// var array5 = this.selectedCalories;
// console.log(array5)
// var index5 = array5.indexOf(Number(itemmm['calories']));
// if (index5 > -1) {
//   array5.splice(index5, 1);
// }

// this.selectedFat.slice(parseInt(item['fat']));
// this.selectedCarbs.push(parseInt(item['carbs']));
// this.selectedProtine.push(parseInt(item['protein']));
// this.selectedCalories.push(parseInt(item['calories']));
this.totalFat=0;
this.totalCarbs=0;
this.totalProtien=0;
this.totalCalories=0;



// for(let k =0; k<this.selectedFat.length; k++){
//   this.totalFat += this.selectedFat[k];

//  }
//  for(let j =0; j<this.selectedCarbs.length; j++){
//   this.totalCarbs += this.selectedCarbs[j];

//  }
//  for(let i =0; i<this.selectedProtine.length; i++){
//   this.totalProtien += this.selectedProtine[i];

//  }

//  for(let m =0; m<this.selectedCalories.length; m++){
//   this.totalCalories += this.selectedCalories[m];

//  }

    //this.dietPlan['days'][this.day][this.meal].foodItems.pop(item);

    for(let x =0; x<this.dietPlan['days'].length;x++){
      for(let m =0; m<this.dietPlan['days'][this.day][x].foodItems.length; m++){
        this.totalCalories += this.dietPlan['days'][this.day][x].foodItems[m]['calories'];
      
       }
      }
  
       //this.totalCarbs=0;
       //this.dietPlan['days'][this.day][this.meal].foodItems[this.indexUsed]['carbs']=this.valueCarbsChanged
       for(let w =0; w<this.dietPlan['days'].length;w++){
       for(let p =0; p<this.dietPlan['days'][this.day][w].foodItems.length; p++){
         this.totalCarbs += this.dietPlan['days'][this.day][w].foodItems[p]['carbs'];
       
        }
      }
  
        //this.totalFat=0;
        //this.dietPlan['days'][this.day][this.meal].foodItems[this.indexUsed]['fat']=this.valuefatChanged
        for(let q =0; q<this.dietPlan['days'].length;q++){
        for(let r =0; r<this.dietPlan['days'][this.day][q].foodItems.length; r++){
         this.totalFat += this.dietPlan['days'][this.day][q].foodItems[r]['fat'];
       
        }
      }
        //this.totalProtien=0;
       //this.dietPlan['days'][this.day][this.meal].foodItems[this.indexUsed]['protien']=this.valueProtienChanged
       for(let x =0; x<this.dietPlan['days'].length;x++){
       for(let s =0; s<this.dietPlan['days'][this.day][x].foodItems.length; s++){
         this.totalProtien += this.dietPlan['days'][this.day][x].foodItems[s]['protien'];
       
        }
      }

  }


  selectedFood: any = [[[]]];
  fat = [];
  protein = [];
  carbs = [];
  fatsum;
  proteinsum;
  carbssum;
  day = 0;
  meal = 0;
  
  dietPlan = {


  };
  dayCount = [];
  mealCount = [];
  currentMealCount = [];

  selectDay(day) {
  //   this.selectedFat =[];
  //   this.selectedCarbs=[];
  //   this.selectedProtine=[];
  //   this.selectedCalories=[];
    this.totalFat=0;
  this.totalCarbs=0;
  this.totalProtien=0;
  this.totalCalories=0;
  this.selectDayfirst=true;

  $("li").removeClass("current2")
  $("#day_" + (day+1)).addClass("current2");
  //this.day = day;

  //this.currentMealCount = this.dietPlan
  this.currentMealCount = [];
  let i=1;
  this.dietPlan['days'][day].forEach(
    day=>{

      this.currentMealCount.push(i);
      i=i+1;


    }
  )
  
  $("#selected_day").val(day - parseInt('1'));
  this.foodItems = [];
  this.day=day;
  //console.log(this.currentMealCount);
  console.log(this.dietPlan);
   
  for(let x =0; x<this.dietPlan['days'][this.day].length;x++){
    //this.totalCalories=0;
      
    for(let m =0; m<this.dietPlan['days'][this.day][x].foodItems.length; m++){
      
      this.totalCalories += this.dietPlan['days'][this.day][x].foodItems[m]['calories'];
    
     }
   

     //this.totalCarbs=0;
     //this.dietPlan['days'][this.day][this.meal].foodItems[this.indexUsed]['carbs']=this.valueCarbsChanged
    //  for(let w =0; w<this.dietPlan['days'][this.day].length;w++){
     for(let p =0; p<this.dietPlan['days'][this.day][x].foodItems.length; p++){
   
       this.totalCarbs += this.dietPlan['days'][this.day][x].foodItems[p]['carbs'];
    
      }
    // }

      //this.totalFat=0;
      //this.dietPlan['days'][this.day][this.meal].foodItems[this.indexUsed]['fat']=this.valuefatChanged
      // for(let q =0; q<this.dietPlan['days'][this.day].length;q++){
      for(let r =0; r<this.dietPlan['days'][this.day][x].foodItems.length; r++){
       this.totalFat += this.dietPlan['days'][this.day][x].foodItems[r]['fat'];
     
      // }
    }
      //this.totalProtien=0;
     //this.dietPlan['days'][this.day][this.meal].foodItems[this.indexUsed]['protien']=this.valueProtienChanged
    //  for(let x =0; x<this.dietPlan['days'][this.day].length;x++){
     for(let s =0; s<this.dietPlan['days'][this.day][x].foodItems.length; s++){
       this.totalProtien += this.dietPlan['days'][this.day][x].foodItems[s]['protien'];
     
      // }
    }
  }
  }

selectMealFirst:boolean;
  selectMeal(meal_val) {
    debugger
    this.selectDayfirst=true;
    this.selectMealFirst =true;
    this.showNotesText=false;
    //this.MealNotes="";
    this.meal = meal_val;
    //this.day = $("#selected_day").val();

    $(".meal_tab").removeClass("current2");

    $("#meal_" + (meal_val+1)).addClass("current2");
    $("#mealWiseDiv").show;

    $("#selected_meal").val(meal_val+1);

    console.log(this.day + "<>" + this.meal);

    console.log(this.dietPlan['days'][this.day][this.meal]);


    if (this.dietPlan['days'][this.day][this.meal] != null || this.dietPlan['days'][this.day][this.meal] != undefined) {
      this.foodItems = this.dietPlan['days'][this.day][this.meal].foodItems;
    }
    else {
      this.foodItems = [];
    }

    

  }


  //Add Day
  addDay() {
    //alert("select Diet Type also.")
    this.selectMealFirst= false;
    if (this.dietPlan['days'] != null) {


      this.dietPlan['days'].push([]);
      this.dayCount.push(this.dietPlan['days'].length);

    }
    else {

      this.dietPlan['days'] = [];
      this.dietPlan['days'].push([]);
      this.dayCount.push(1);

    }

  }
showNotesText=false;
  addNotes(){
    this.showNotesText=true;
debugger
    //this.dietPlan['days'][this.day][this.meal].notes=this.MealNotes;
    for(let i =0;i<this.dietPlan['days'][this.day].length;i++){
      this.MealNotes = this.dietPlan['days'][i][this.meal].notes;
    }

  }
  postNotes(){
    // this.showNotesText=true;
debugger

let mealnote:any=((<HTMLInputElement>document.getElementById('meal-desc1')).value)

    this.dietPlan['days'][this.day][this.meal].notes=mealnote;
   
  }


  getMealIndex(item) {

    return item.day === this.day;

  }

  getMealCount() {
    if (this.mealCount != null)
      if (this.mealCount[this.day] != null)
        return this.mealCount[this.day].noOfMeals
    return 0;
  }

  //Add Meal
  MealNotes;

  addMeal() {
if(this.selectDayfirst==true){
   // this.day =$("#selected_day").val();
   if (this.dietPlan['days'] != null) {

    if (this.dietPlan['days'][this.day] != null) {

      this.dietPlan['days'][this.day].push({})
      let index = this.mealCount.findIndex((item) => {
        return item.day === this.day;
      })
      if (index > -1) {


        this.mealCount[index].noOfMeals = this.dietPlan['days'][this.day].length;
        this.currentMealCount = []
        //this.dietPlan['days'][this.day]['notes']= this.MealNotes;
        for (let i = 1; i <= this.mealCount[index].noOfMeals; i++)
          this.currentMealCount.push(i)

      }
      else {
        //  alert(3 +"<>"+this.day);

        this.mealCount[0] = {};
        this.mealCount[0].day = this.day;
        this.mealCount[0].noOfMeals = this.dietPlan['days'][this.day].length;
        this.currentMealCount = [1];
      }

    }
    else {
      // alert(4);

      this.dietPlan['days'][this.day] = [];
      this.dietPlan['days'][this.day].push({})
      this.mealCount[0] = {};
      this.mealCount[0].noOfMeals = 1;
      this.mealCount[0].day = this.day;
      this.currentMealCount = [1];


    }




  }

}else{
  alert("Add day first")
}

  }

  RemoveMeal() {
    debugger

    this.dietPlan['days'][this.day].splice( this.dietPlan['days'][this.day].length-1,1);
    
    this.currentMealCount = [];
   
    for (let i = 1; i <= this.dietPlan['days'][this.day].length; i++)
          this.currentMealCount.push(i)
   
    console.log(this.dietPlan);
  }

  allowDrop(ev) {
    ev.preventDefault();
  }

  ViewPreview(){
debugger
    localStorage.setItem('dietplan', JSON.stringify(this.dietPlan));
    //this._router.navigate(['/#/pages/dait-preview']);
    window.location.href="/#/pages/dait-preview";
    // window.open("/#/pages/dait-preview", "_blank");
  }
  dietplanDay =[];
  dietplanMeal=[];
  dietPlanPreviewLoop = [];
  getDietPlanPreview() {
    debugger
    //this.dietPlan = JSON.parse(localStorage.getItem('dietplan'));
    this.dietplanDay = this.dietPlan['days'];


    // if (this.dietPlan['days'] != null) {


    //   //this.dietPlan['days'].push([]);
    //   this.dayCount.push(this.dietPlan['days'].length);

    // }

    for (let i = 0; i < this.dietPlan['days'].length; i++) {
      // this.dayscount.push(this.dietPlanPreview['days'].length);

      for (let j = 0; j < this.dietPlan['days'][i].length; j++) {
        //this.dietplanMeal[j] = this.dietPlan['days'][j];
        this.dietplanMeal.push(this.dietPlan['days'][i][j]);

        for (let l = 0; l < this.dietPlan['days'][i][j].foodItems.length; l++) {
            

                      this.dietPlanPreviewLoop.push(this.dietPlan['days'][i][j].foodItems[l]);
        
          //             this.foodName = this.dietPlanPreview['days'][i][j].foodItems[l].foodName;
          //             this.calories = this.dietPlanPreview['days'][i][j].foodItems[l].calories;
          //             this.portion = this.dietPlanPreview['days'][i][j].foodItems[l].portion;
          //             this.unit = this.dietPlanPreview['days'][i][j].foodItems[l].unit;
        
        
                    
        
                  
        
              }

      }
    }
  // //       this.mealcount = [];
  // // let i=1;
  // // this.dietPlanPreview['days'][i].forEach(
  // //   day=>{

  // //     this.mealcount.push(i);
  // //     i=i+1;


  // //   }
  // // )

  //         for (let l = 0; l < this.dietPlanPreview['days'][i][j].foodItems.length; l++) {
            

  //             this.dietPlanPreviewLoop.push(this.dietPlanPreview['days'][i][j].foodItems[l]);

  //             this.foodName = this.dietPlanPreview['days'][i][j].foodItems[l].foodName;
  //             this.calories = this.dietPlanPreview['days'][i][j].foodItems[l].calories;
  //             this.portion = this.dietPlanPreview['days'][i][j].foodItems[l].portion;
  //             this.unit = this.dietPlanPreview['days'][i][j].foodItems[l].unit;


            

          

  //       }

  //     }

  //   }

    console.log(this.dietPlanPreviewLoop);

  }


  totalFat=0;
  totalCarbs=0;
  totalProtien=0;
  totalCalories=0;
  selectedCalories=[];
  selectedFat = [];
  selectedCarbs = [];
  selectedProtine = [];
  unit;
  getValue = (item) => {
    // this.item=item;
     debugger

     if(this.selectDayfirst==true && this.selectMealFirst ==true){
    // this.selectedFat.push(Number(item['fat']));
    // this.selectedCarbs.push(Number(item['carbs']));
    // this.selectedProtine.push(Number(item['protein']));
    // this.selectedCalories.push(Number(item['calories']));

    // for(let k =0; k<this.selectedFat.length; k++){
    //   //this.totalFat += this.selectedFat[k];
    //   this.totalFat = this.selectedFat.reduce((a, b) => a + b, 0);
   
    //  }
    //  for(let j =0; j<this.selectedCarbs.length; j++){
    //  //this.totalCarbs += this.selectedCarbs[j];
    //   this.totalCarbs = this.selectedCarbs.reduce((a, b) => a + b, 0);
   
    //  }
    //  for(let i =0; i<this.selectedProtine.length; i++){
    //   //this.totalProtien += this.selectedProtine[i];
    //   this.totalProtien = this.selectedProtine.reduce((a, b) => a + b, 0);
   
    //  }

    //  for(let m =0; m<this.selectedProtine.length; m++){
    //   //this.totalCalories += this.selectedCalories[m];
    //   this.totalCalories = this.selectedCalories.reduce((a, b) => a + b, 0);
   
    //  }

    console.log(item);
    let items = {};
    //this.unit=item['unit'];
debugger
    items['foodID'] = item['_id'];
    items['foodName'] = item['name'];
    items['portion'] = parseInt(item['quantity']);
    items['calories'] = item['calories'];
    items['fat'] = Number(item['fat']);
    if(item['protein']){
      items['protien'] = item['protein'];}else{
        items['protien'] = item['protien'];
      }
    
    items['carbs'] = Number(item['carbs']);
    items['unit'] = item['unit'];
    //this.foodItems = this.dietPlan['days'][this.day][this.meal].foodItems;
    // for(let i=0;i<this.foodItems.length;i++)
    // if(item['_id']== this.foodItems[i]['foodID'] ){
    //   alert("this foodItem is Already added for this meal")
    // }else{}

    if (this.dietPlan['days'] != null) {

      if (this.dietPlan['days'][this.day] != null) {
        if (this.dietPlan['days'][this.day][this.meal] != null) {

          let mealArray = this.dietPlan['days'][this.day][this.meal]
          if (mealArray['foodItems'] != null) {
            mealArray['foodItems'].push(items);
          }
          else {
            mealArray['foodItems'] = [];
            mealArray['foodItems'].push(items);
          }
        }
        else {

          this.dietPlan['days'][this.day][this.meal] = {};
          let mealArray = this.dietPlan['days'][this.day][this.meal]
          mealArray['foodItems'] = [];
          mealArray['foodItems'].push(items);
          //this.dietPlan['days'][this.day][this.meal].push(mealArray);

        }



      }
      else {

        this.dietPlan['days'][this.day] = []
        this.dietPlan['days'][this.day][this.meal] = {};
        let mealArray = this.dietPlan['days'][this.day][this.meal]
        mealArray['foodItems'] = [];
        mealArray['foodItems'].push(items);
        //this.dietPlan['days'][this.day][this.meal].push(mealArray);


      }




    }
    else {
      //alert('please add day first');
      this.dietPlan['days'] = [];
      this.dietPlan['days'][this.day] = []
      this.dietPlan['days'][this.day][this.meal] = {};
      let mealArray = this.dietPlan['days'][this.day][this.meal]
      mealArray['foodItems'] = [];
      mealArray['foodItems'].push(items);
     
      //this.dietPlan['days'][this.day][this.meal].push(mealArray);


    }
  }else{
    alert('please Select day And Meal first');
  }
    //console.log(this.dietPlan);
    //    this.selectedFood[this.day][this.meal].push(item);

    //this.day = $("#selected_day").val();
    //this.meal = $("#selected_meal").val();

    console.log(this.foodItems);
    console.log("-----------------");
    //alert(this.day +"<>"+this.meal);
    this.foodItems = this.dietPlan['days'][this.day][this.meal].foodItems;

    this.totalCalories=0;
    // this.dietPlan['days'][this.day][this.meal].foodItems[this.indexUsed]['calories']=this.valueCaloriesChanged;
    console.log(this.dietPlan['days'][this.day]);

    for(let x =0; x<this.dietPlan['days'][this.day].length;x++){
      
    for(let m =0; m<this.dietPlan['days'][this.day][x].foodItems.length; m++){
      
      this.totalCalories += this.dietPlan['days'][this.day][x].foodItems[m]['calories'];
    
     }
    }

     this.totalCarbs=0;
     //this.dietPlan['days'][this.day][this.meal].foodItems[this.indexUsed]['carbs']=this.valueCarbsChanged
      for(let w =0; w<this.dietPlan['days'][this.day].length;w++){
     for(let p =0; p<this.dietPlan['days'][this.day][w].foodItems.length; p++){
   
       this.totalCarbs += this.dietPlan['days'][this.day][w].foodItems[p]['carbs'];
    
      }
     }

      this.totalFat=0;
      //this.dietPlan['days'][this.day][this.meal].foodItems[this.indexUsed]['fat']=this.valuefatChanged
      for(let q =0; q<this.dietPlan['days'][this.day].length;q++){
      for(let r =0; r<this.dietPlan['days'][this.day][q].foodItems.length; r++){
       this.totalFat += this.dietPlan['days'][this.day][q].foodItems[r]['fat'];
     
      }
    }
      this.totalProtien=0;
     //this.dietPlan['days'][this.day][this.meal].foodItems[this.indexUsed]['protien']=this.valueProtienChanged
     for(let x =0; x<this.dietPlan['days'][this.day].length;x++){
     for(let s =0; s<this.dietPlan['days'][this.day][x].foodItems.length; s++){
       this.totalProtien += this.dietPlan['days'][this.day][x].foodItems[s]['protien'];
     
      }
    
  }
    console.log(this.foodItems);
    // this.fat.push(item['fat']);
    // this.protein.push(item['protein']);
    // this.carbs.push(item['carbs']);

    // this.fatsum = this.fat.reduce(add, 0);

    // // function add(a, b) {
    // //   return a + b;
    // }


    // this.proteinsum = this.protein.reduce(add1, 0);

    // function add1(a, b) {
    //   return a + b;
    // }


    // this.carbssum = this.carbs.reduce(add2, 0);

    // function add2(a, b) {
    //   return a + b;
    // }



    //  let localUrl = "diet/add";
    //  let url = this.server_url+localUrl;
    //  this.http.post(url,dietobject,this.httpOptions)
    //  .subscribe(
    //     (res) => {
    //       // if(res['status']== 'success') {
    //       //  alert("You added data successfully..");
    //       // //  this.viewProduct ='';
    //       // // $("#myModal").modal("hide");
    //       //  this.getDatabookexpense();
    //       //     }else {
    //       //       alert("Product NOT Added");
    //       //     }
    //     },
    //     err => {alert( "Error occoured");
    //    // this.viewAuditor ='';
    //   }
    //     );
    
  }
  // sel_day;
  DuplicateDay() {
    debugger


    this.dietPlan['days'].push( this.dietPlan['days'][this.dietPlan['days'].length-1]);
    
    this.dayCount = [];
    for (let i = 1; i < this.dietPlan['days'].length + 1; i++)
      this.dayCount.push(i)


    /*let arrEmptyIndexes = []
    let ctr = 0

    this.dietPlan['days'].forEach(day => {

      if (day[0].foodItems == null)
        arrEmptyIndexes.push(ctr);
      ctr = ctr + 1;

    })


    arrEmptyIndexes.forEach(index => {

      this.dietPlan['days'].splice(index, 1);


    })

    //this.sel_day = $("#selected_day").val();

    this.dietPlan['days'].push(this.dietPlan['days'][this.day]);
    this.dayCount = [];
    for (let i = 2; i <= this.dietPlan['days'].length + 1; i++)
      this.dayCount.push(i)

    // this.dietPlan['days'][this.sel_day][this.meal].foodItems
    // for (let i = 1; i <= this.mealCount[index].noOfMeals; i++)
    // this.currentMealCount.push(i)

    //this.dayCount.push(this.dietPlan['days'].length);
    // this.currentMealCount = [3];
    console.log(this.dietPlan);*/
  }

  // sel__remove_day;
  
  RemoveDay() {
    debugger

    this.dietPlan['days'].splice( this.dietPlan['days'][this.dietPlan['days'].length-1],1);
    
    this.dayCount = [];
    for (let i = 1; i < this.dietPlan['days'].length + 1; i++)
      this.dayCount.push(i)
   
    console.log(this.dietPlan);
  }

  
  selectedDay: any = [];
  selectedMeal: any = [];
  //dietPlan;
  dietPlanDay;
  //days=[];
  
  public postDiet(): void {
 

    let arrEmptyIndexes = []
    let ctr = 0

    this.dietPlan['days'].forEach(day => {

      if (day[0].foodItems == null)
        arrEmptyIndexes.push(ctr);
      ctr = ctr + 1;

    })


    arrEmptyIndexes.forEach(index => {

      this.dietPlan['days'].splice(index, 1);


    })


    // this.diet_type1 =document.getElementById('dietType1').innerHTML;
    // this.diet_type2 =document.getElementById('dietType2').innerHTML;
    // this.diet_type3 =document.getElementById('dietType3').innerHTML;
    // this.diet_type4 =document.getElementById('dietType4').innerHTML;

   


    this.dietPlan['coachID'] = this.coach_id;
    this.dietPlan['header'] = this.header_text;
    this.dietPlan['primeIngredients'] = this.prime_Ingredients;
    this.dietPlan['footer'] = this.footer_text;
    this.dietPlan['description'] = this.Description;
    this.dietPlan['dietPreference'] = this.Diet_Preference;
    this.dietPlan['dietPlanName'] = this.Description;
    this.dietPlan['rangeOfCalories'] = this.Calorie_Range;
    this.dietPlan['primeIngredients'] = this.prime_Ingredients;
    this.dietPlan['dietType'] = this.DietType;

    //this.dietPlan['days'][this.day]['notes']= this.MealNotes;

 
    //this.dietPlan['days'][this.day]['notes']="string";





    //postDietObject.push(['days'][0][0].foodItems.push[this.selectedFood]);



debugger

    let localUrl = "diet/add";
    let url = this.server_url + localUrl;
    this.http.post(url, this.dietPlan, this.httpOptions)
      .subscribe(
        (res) => {

          console.log(JSON.stringify(res))
          let urll = res['DIET'].docPath;
          urll = atob(urll);
          urll = urll.replace('www.mobicloudtechnologies.com', 'http://23.95.106.215:4000');
          // urll = urll.replace('localhost:4200', 'http://23.95.106.215:4000');
          window.open(urll);

          // location.reload();
          //this.ngOnInit();
          // //  this.viewProduct ='';
          // // $("#myModal").modal("hide");
          //this.getDietFoodItem();

        },
        err => {
          alert("Error occoured");
          // this.viewAuditor ='';
        }
      );

  }

  public postAndAssignDiet(): void {
 

    let arrEmptyIndexes = []
    let ctr = 0

    this.dietPlan['days'].forEach(day => {

      if (day[0].foodItems == null)
        arrEmptyIndexes.push(ctr);
      ctr = ctr + 1;

    })


    arrEmptyIndexes.forEach(index => {

      this.dietPlan['days'].splice(index, 1);


    })


    // this.diet_type1 =document.getElementById('dietType1').innerHTML;
    // this.diet_type2 =document.getElementById('dietType2').innerHTML;
    // this.diet_type3 =document.getElementById('dietType3').innerHTML;
    // this.diet_type4 =document.getElementById('dietType4').innerHTML;

   


    this.dietPlan['coachID'] = this.coach_id;
    this.dietPlan['header'] = this.header_text;
    this.dietPlan['primeIngredients'] = this.prime_Ingredients;
    this.dietPlan['footer'] = this.footer_text;
    this.dietPlan['description'] = this.Description;
    this.dietPlan['dietPreference'] = this.Diet_Preference;
    this.dietPlan['dietPlanName'] = this.Description;
    this.dietPlan['rangeOfCalories'] = this.Calorie_Range;
    this.dietPlan['primeIngredients'] = this.prime_Ingredients;
    this.dietPlan['dietType'] = this.DietType;

    //this.dietPlan['days'][this.day]['notes']= this.MealNotes;

 
    //this.dietPlan['days'][this.day]['notes']="string";





    //postDietObject.push(['days'][0][0].foodItems.push[this.selectedFood]);



debugger

    let localUrl = "diet/add";
    let url = this.server_url + localUrl;
    this.http.post(url, this.dietPlan, this.httpOptions)
      .subscribe(
        (res) => {

          console.log(JSON.stringify(res))
          let dietId = res['DIET'].dietId;
          // urll = atob(urll);
          //let Dietid = '5bced65c8790ef0633af8537';
          // // urll = urll.replace('www.mobicloudtechnologies.com', 'http://23.95.106.215:4000');
          // urll = urll.replace('localhost:4200', 'http://23.95.106.215:4000');
          // window.location.href='#/pages/diet-assign/'+dietId;

          window.open(
            '#/pages/diet-assign/'+dietId,
            '_blank' // <- This is what makes it open in a new window.
          );

          // location.reload();
          //this.ngOnInit();
          // //  this.viewProduct ='';
          // // $("#myModal").modal("hide");
          //this.getDietFoodItem();

        },
        err => {
          alert("Error occoured");
          // this.viewAuditor ='';
        }
      );

  }


  valueCalories;
  valuefat;
  valueCarbs;
  valueProtien;
  valueQuantity;
  valuePortion;
  Quantity_value;
  valueUnit;
  itemArray;
  day_array_value;
  day_meal_value;
  foodchanged;
  indexUsed;
  changedUnit;
  changedCalories;
  calfat;
  calfatX;
  calCarbs;
  calCarbsX;
  calProtien;
  calProtienX;
  gmsCallories;
  gmsCalloriesX;
  MLcalfatX;
  MLcalfat;
  MLcalCarbsX;
  MLcalCarbs;
  MLcalProtienX;
  MLcalProtien;
  MlCalloriesX;
  MlCallories;
  Largecalfat;
  LargecalCarbs;
  LargecalProtien;
  LargeCallories;
  valuefatChanged;
  valueCarbsChanged;
  valueProtienChanged;
  valueCaloriesChanged;
  valueCaloriesDecrease;

  ValueCalculation = (item: string, index) => {
    
    $("#Item_calculations").show();
    this.valueCalories = item['calories'];
  this.valueCaloriesDecrease = item['calories'];
    this.valuefat = parseFloat(item['fat']);
    this.valueCarbs = parseFloat(item['carbs']);
    this.valueProtien = item['protien'];
    this.valueQuantity = item['portion'];
    this.valueUnit = item['unit'];
    this.indexUsed = index;
    this.valuefatChanged = this.valuefat;
    this.valueCarbsChanged = this.valueCarbs;
    this.valueProtienChanged = this.valueProtien;
    this.valueCaloriesChanged=this.valueCalories;

  }

  StatCalculation() {
    debugger
     
    this.foodchanged = this.dietPlan['days'][this.day][this.meal].foodItems[this.indexUsed];
    this.changedUnit = this.foodchanged['unit'];
    this.dietPlan['days'][this.day][this.meal].foodItems[this.indexUsed]['portion'] = this.Quantity_value;
    
    

    
    switch (this.valueUnit) {
          
      case 'gms':

        this.calfatX = this.valuefat / this.valueQuantity;
        this.calfat = this.calfatX * this.Quantity_value;
        this.valuefatChanged = this.calfat;

        this.calCarbsX = this.valueCarbs / this.valueQuantity;
        this.calCarbs = this.calCarbsX * this.Quantity_value;

        this.valueCarbsChanged = this.calCarbs;

        this.calProtienX = this.valueProtien / this.valueQuantity;
        this.calProtien = this.calProtienX * this.Quantity_value;

        this.valueProtienChanged = this.calProtien;

        this.gmsCalloriesX = this.valueCalories / this.valueQuantity;
        this.gmsCallories = this.gmsCalloriesX * this.Quantity_value;
        this.dietPlan['days'][this.day][this.meal].foodItems[this.indexUsed]['calories'] = this.gmsCallories;
        this.valueCaloriesChanged = this.gmsCallories;
        break;

      case 'ml':
        this.MLcalfatX = this.valuefat / this.valueQuantity;
        this.MLcalfat = this.MLcalfatX * this.Quantity_value;


        this.valuefatChanged = this.MLcalfat;

        this.MLcalCarbsX = this.valueCarbs / this.valueQuantity;
        this.MLcalCarbs = this.MLcalCarbsX * this.Quantity_value;

        this.valueCarbsChanged = this.MLcalCarbs;

        this.MLcalProtienX = this.valueProtien / this.valueQuantity;
        this.MLcalProtien = this.MLcalProtienX * this.Quantity_value;

        this.valueProtienChanged = this.MLcalProtien;
        this.MlCalloriesX = this.valueCalories / this.valueQuantity;
        this.MlCallories = this.MlCalloriesX * this.Quantity_value;
        this.dietPlan['days'][this.day][this.meal].foodItems[this.indexUsed]['calories'] = this.MlCallories;
        this.valueCaloriesChanged = this.MlCallories;
        break;

      case 'Serving':
        //this.MLcalfatX = this.valuefat / 4;
        this.MLcalfat = this.MLcalfatX * this.Quantity_value;


        this.valuefatChanged = this.MLcalfat;

        //this.MLcalCarbsX = this.valueCarbs / 4;
        this.MLcalCarbs = this.valueCarbs * this.Quantity_value;

        this.valueCarbsChanged = this.MLcalCarbs;

        //this.MLcalProtienX = this.valueProtien / 4;
        this.MLcalProtien = this.valueProtien * this.Quantity_value;

        this.valueProtienChanged = this.MLcalProtien;
        //this.MlCalloriesX = this.valueCalories / 4;
        this.MlCallories = this.valueCalories * this.Quantity_value;
        this.dietPlan['days'][this.day][this.meal].foodItems[this.indexUsed]['calories'] = this.MlCallories;
        this.valueCaloriesChanged = this.MlCallories;
        break;

        case 'serving':
        //this.MLcalfatX = this.valuefat / 4;
        this.MLcalfat = this.MLcalfatX * this.Quantity_value;


        this.valuefatChanged = this.MLcalfat;

        //this.MLcalCarbsX = this.valueCarbs / 4;
        this.MLcalCarbs = this.valueCarbs * this.Quantity_value;

        this.valueCarbsChanged = this.MLcalCarbs;

        //this.MLcalProtienX = this.valueProtien / 4;
        this.MLcalProtien = this.valueProtien * this.Quantity_value;

        this.valueProtienChanged = this.MLcalProtien;
        //this.MlCalloriesX = this.valueCalories / 4;
        this.MlCallories = this.valueCalories * this.Quantity_value;
        this.dietPlan['days'][this.day][this.meal].foodItems[this.indexUsed]['calories'] = this.MlCallories;
        this.valueCaloriesChanged = this.MlCallories;
        break;

      case 'salad':
        this.MLcalfatX = this.valuefat / 4;
        this.MLcalfat = this.MLcalfatX * this.Quantity_value;


        this.valuefatChanged = this.MLcalfat;

        this.MLcalCarbsX = this.valueCarbs / 4;
        this.MLcalCarbs = this.MLcalCarbsX * this.Quantity_value;

        this.valueCarbsChanged = this.MLcalCarbs;

        this.MLcalProtienX = this.valueProtien / 4;
        this.MLcalProtien = this.MLcalProtienX * this.Quantity_value;

        this.valueProtienChanged = this.MLcalProtien;
        this.MlCalloriesX = this.valueCalories / 4;
        this.MlCallories = this.MlCalloriesX * this.Quantity_value;
        this.dietPlan['days'][this.day][this.meal].foodItems[this.indexUsed]['calories'] = this.MlCallories;
        this.valueCaloriesChanged = this.MlCallories;
        break;

        case 'Salad':
        this.MLcalfatX = this.valuefat / 4;
        this.MLcalfat = this.MLcalfatX * this.Quantity_value;


        this.valuefatChanged = this.MLcalfat;

        this.MLcalCarbsX = this.valueCarbs / 4;
        this.MLcalCarbs = this.MLcalCarbsX * this.Quantity_value;

        this.valueCarbsChanged = this.MLcalCarbs;

        this.MLcalProtienX = this.valueProtien / 4;
        this.MLcalProtien = this.MLcalProtienX * this.Quantity_value;

        this.valueProtienChanged = this.MLcalProtien;
        this.MlCalloriesX = this.valueCalories / 4;
        this.MlCallories = this.MlCalloriesX * this.Quantity_value;
        this.dietPlan['days'][this.day][this.meal].foodItems[this.indexUsed]['calories'] = this.MlCallories;
        this.valueCaloriesChanged = this.MlCallories;
        break;


      case 'inch':
        this.MLcalfatX = this.valuefat / this.valueQuantity;
        this.MLcalfat = this.MLcalfatX * this.Quantity_value;


        this.valuefatChanged = this.MLcalfat;

        this.MLcalCarbsX = this.valueCarbs / this.valueQuantity;
        this.MLcalCarbs = this.MLcalCarbsX * this.Quantity_value;

        this.valueCarbsChanged = this.MLcalCarbs;

        this.MLcalProtienX = this.valueProtien /this.valueQuantity;
        this.MLcalProtien = this.MLcalProtienX * this.Quantity_value;

        this.valueProtienChanged = this.MLcalProtien;

        this.MlCalloriesX = this.valueCalories / this.valueQuantity;
        this.MlCallories = this.MlCalloriesX * this.Quantity_value;
        this.dietPlan['days'][this.day][this.meal].foodItems[this.indexUsed]['calories'] = this.MlCallories;
        this.valueCaloriesChanged = this.MlCallories;

        break;

        case 'Slice':
        //this.MLcalfatX = this.valuefat / 4;
        this.MLcalfat = this.valuefat * this.Quantity_value;


        this.valuefatChanged = this.MLcalfat;

        //this.MLcalCarbsX = this.valueCarbs / 4;
        this.MLcalCarbs = this.valueCarbs * this.Quantity_value;

        this.valueCarbsChanged = this.MLcalCarbs;

        //this.MLcalProtienX = this.valueProtien / 4;
        this.MLcalProtien = this.valueProtien * this.Quantity_value;

        this.valueProtienChanged = this.MLcalProtien;

        //this.MlCalloriesX = this.valueCalories / 4;
        this.MlCallories = this.valueCalories * this.Quantity_value;
        this.dietPlan['days'][this.day][this.meal].foodItems[this.indexUsed]['calories'] = this.MlCallories;
        this.valueCaloriesChanged = this.MlCallories;

        break;
        case 'slice':
        //this.MLcalfatX = this.valuefat / 4;
        this.MLcalfat = this.valuefat * this.Quantity_value;


        this.valuefatChanged = this.MLcalfat;

        //this.MLcalCarbsX = this.valueCarbs / 4;
        this.MLcalCarbs = this.valueCarbs * this.Quantity_value;

        this.valueCarbsChanged = this.MLcalCarbs;

        //this.MLcalProtienX = this.valueProtien / 4;
        this.MLcalProtien = this.valueProtien * this.Quantity_value;

        this.valueProtienChanged = this.MLcalProtien;

        //this.MlCalloriesX = this.valueCalories / 4;
        this.MlCallories = this.valueCalories * this.Quantity_value;
        this.dietPlan['days'][this.day][this.meal].foodItems[this.indexUsed]['calories'] = this.MlCallories;
        this.valueCaloriesChanged = this.MlCallories;

        break;

        case 'portion':
        //this.MLcalfatX = this.valuefat / 4;
        this.MLcalfat = this.valuefat * this.Quantity_value;


        this.valuefatChanged = this.MLcalfat;

        //this.MLcalCarbsX = this.valueCarbs / 4;
        this.MLcalCarbs = this.valueCarbs * this.Quantity_value;

        this.valueCarbsChanged = this.MLcalCarbs;

        //this.MLcalProtienX = this.valueProtien / 4;
        this.MLcalProtien = this.valueProtien * this.Quantity_value;

        this.valueProtienChanged = this.MLcalProtien;

        //this.MlCalloriesX = this.valueCalories / 4;
        this.MlCallories = this.valueCalories * this.Quantity_value;
        this.dietPlan['days'][this.day][this.meal].foodItems[this.indexUsed]['calories'] = this.MlCallories;
        this.valueCaloriesChanged = this.MlCallories;

        break;


      case 'scoop':

         if(this.Quantity_value <= 2)
         {

        this.MLcalfatX = this.valuefat * this.Quantity_value;
       
        this.valuefatChanged = this.MLcalfatX;

        this.MLcalCarbsX = this.valueCarbs * this.Quantity_value;
        this.valueCarbsChanged = this.MLcalCarbsX ;

        this.valueCarbs = this.MLcalCarbsX;

        this.MLcalProtienX = this.valueProtien * this.Quantity_value;
        this.MLcalProtien = this.MLcalProtienX ;

        this.valueProtienChanged = this.MLcalProtienX;
        this.MlCalloriesX = this.valueCalories * this.Quantity_value;
        this.MlCallories = this.MlCalloriesX;
        this.dietPlan['days'][this.day][this.meal].foodItems[this.indexUsed]['calories'] = this.MlCallories;
        this.valueCaloriesChanged = this.MlCallories;
         }
         else{
           alert('please Enter Value Between (0.5 , 1.0, 1.5, 2.0)');
         }

        break;

      case 'large':

        this.Largecalfat = this.valuefat * this.Quantity_value;


        this.valuefatChanged = this.Largecalfat;


        this.LargecalCarbs = this.valueCarbs * this.Quantity_value;

        this.valueCarbsChanged = this.LargecalCarbs;


        this.LargecalProtien = this.valueProtien * this.Quantity_value;

        this.valueProtienChanged = this.LargecalProtien;


        this.LargeCallories = this.valueCalories * this.Quantity_value;
        this.dietPlan['days'][this.day][this.meal].foodItems[this.indexUsed]['calories'] = this.LargeCallories;
        this.valueCaloriesChanged = this.LargeCallories;

        break;


    }
     debugger
    //      this.totalCalories = this.totalCalories - this.valueCaloriesChanged;
    // debugger
     
    //  var array = this.dietPlan['days'][this.day][this.meal].foodItems[this.indexUsed]['calories'];
    //  var index = this.selectedCalories.indexOf(array);
    // if (~index) {
    //   this.selectedCalories[index] = this.valueCaloriesChanged;
    // }
    // this.selectedFat.push(Number(this.valueCaloriesChanged));
    // this.selectedCarbs.push(Number(item['carbs']));
    // this.selectedProtine.push(Number(item['protein']));
    // if (index !== -1) {
    // this.selectedCalories[index]=array;
    this.totalCalories=0;
    // this.dietPlan['days'][this.day][this.meal].foodItems[this.indexUsed]['calories']=this.valueCaloriesChanged;
    console.log(this.dietPlan['days'][this.day]);
    for(let x =0; x<this.dietPlan['days'][this.day].length;x++){
    for(let m =0; m<this.dietPlan['days'][this.day][x].foodItems.length; m++){
      this.totalCalories += this.dietPlan['days'][this.day][x].foodItems[m]['calories'];
    
     }
    }

     this.totalCarbs=0;
     this.dietPlan['days'][this.day][this.meal].foodItems[this.indexUsed]['carbs']=this.valueCarbsChanged
     for(let w =0; w<this.dietPlan['days'][this.day].length;w++){
     for(let p =0; p<this.dietPlan['days'][this.day][w].foodItems.length; p++){
       this.totalCarbs += this.dietPlan['days'][this.day][w].foodItems[p]['carbs'];
     
      }
    }

      this.totalFat=0;
      this.dietPlan['days'][this.day][this.meal].foodItems[this.indexUsed]['fat']=this.valuefatChanged
      for(let q =0; q<this.dietPlan['days'][this.day].length;q++){
      for(let r =0; r<this.dietPlan['days'][this.day][q].foodItems.length; r++){
       this.totalFat += this.dietPlan['days'][this.day][q].foodItems[r]['fat'];
     
      }
    }
      this.totalProtien=0;
     this.dietPlan['days'][this.day][this.meal].foodItems[this.indexUsed]['protien']=this.valueProtienChanged
     for(let x =0; x<this.dietPlan['days'][this.day].length;x++){
     for(let s =0; s<this.dietPlan['days'][this.day][x].foodItems.length; s++){
       this.totalProtien += this.dietPlan['days'][this.day][x].foodItems[s]['protien'];
     
      }
    }
    // }
    

    
    // var array2 = this.selectedFat;
    // console.log(array2)
    // var index2 = array2.indexOf(Number(this.foodchanged['fat']));
    // if (index2 > -1) {
    //   array2.splice(index2, 1);
    // }
    
    // var array3 = this.selectedCarbs;
    // console.log(array3)
    // var index3 = array3.indexOf(Number(itemmm['carbs']));
    // if (index3 > -1) {
    //   array3.splice(index3, 1);
    // }
    
    // var array4 = this.selectedProtine;
    // console.log(array4)
    // var index4 = array4.indexOf(Number(itemmm['protien']));
    // if (index4 > -1) {
    //   array4.splice(index4, 1);
    // }
    
    // var array5 = this.selectedCalories;
    // console.log(array5)
    // var index5 = array5.indexOf(Number(itemmm['calories']));
    // if (index5 > -1) {
    //   array5.splice(index5, 1);
    // }

  }

  DietType;
  statCalories=1000;
  ketoFat;
  ketoProtein;
  ketoCarbs;
  //LOWCARBCalories=1000;
  LOWCARBFat;
  LOWCARBProtein;
  LOWCARBCarbs;
  //MODERATECARBCalories=1000;
  MODERATECARBFat;
  MODERATECARBProtein;
  MODERATECARBCarbs;
  //HIGHCARBCalories=1000;
  HIGHCARBFat;
  HIGHCARBProtein;
  HIGHCARBCarbs;
  //ketoCaloriesTexPercent:number;
  DietTypeChange(type){
    debugger
    this.DietType =type;
    if(this.DietType=='KETOGENIC'){
      //this.ketoCalories=1000;
      let ketoCaloriesTexPercent:any = ((<HTMLInputElement>document.getElementById('ketocal')).value);
      this.statCalories= (this.statCalories*ketoCaloriesTexPercent/100)*1;

      let ketoFatTexPercent:any = ((<HTMLInputElement>document.getElementById('ketofat')).value);
      this.ketoFat= ((this.statCalories*ketoFatTexPercent/100)/9).toFixed(2);

      let ketoProteinTexPercent:any = ((<HTMLInputElement>document.getElementById('ketoprotein')).value);
      this.ketoProtein= ((this.statCalories*ketoProteinTexPercent/100)/4).toFixed(2);

      let ketoCarbsTexPercent:any = ((<HTMLInputElement>document.getElementById('ketocarbs')).value);
      this.ketoCarbs= ((this.statCalories*ketoCarbsTexPercent/100)/4).toFixed(2);

    }

    if(this.DietType=='LOW_CARB'){
      //this.LOWCARBCalories=1000;
      let LOWCARBCaloriesTexPercent:any = ((<HTMLInputElement>document.getElementById('LOWCARBcal')).value);
      this.statCalories= (this.statCalories*LOWCARBCaloriesTexPercent/100)*1;

      let LOWCARBFatTexPercent:any = ((<HTMLInputElement>document.getElementById('LOWCARBfat')).value);
      this.LOWCARBFat= ((this.statCalories*LOWCARBFatTexPercent/100)/9).toFixed(2);

      let LOWCARBProteinTexPercent:any = ((<HTMLInputElement>document.getElementById('LOWCARBprotein')).value);
      this.LOWCARBProtein= ((this.statCalories*LOWCARBProteinTexPercent/100)/4).toFixed(2);

      let LOWCARBCarbsTexPercent:any = ((<HTMLInputElement>document.getElementById('LOWCARBcarbs')).value);
      this.LOWCARBCarbs= ((this.statCalories*LOWCARBCarbsTexPercent/100)/4).toFixed(2);

    }

    if(this.DietType=='MODERATE_CARB'){
      //this.LOWCARBCalories=1000;
      let MODERATECARBCaloriesTexPercent:any = ((<HTMLInputElement>document.getElementById('MODERATECARBcal')).value);
      this.statCalories= (this.statCalories*MODERATECARBCaloriesTexPercent/100)*1;

      let MODERATECARBFatTexPercent:any = ((<HTMLInputElement>document.getElementById('MODERATECARBfat')).value);
      this.MODERATECARBFat= ((this.statCalories*MODERATECARBFatTexPercent/100)/9).toFixed(2);

      let MODERATECARBProteinTexPercent:any = ((<HTMLInputElement>document.getElementById('MODERATECARBprotein')).value);
      this.MODERATECARBProtein= ((this.statCalories*MODERATECARBProteinTexPercent/100)/4).toFixed(2);

      let MODERATECARBCarbsTexPercent:any = ((<HTMLInputElement>document.getElementById('MODERATECARBcarbs')).value);
      this.MODERATECARBCarbs= ((this.statCalories*MODERATECARBCarbsTexPercent/100)/4).toFixed(2);

    }

    if(this.DietType=='HIGH_CARB'){
      //this.LOWCARBCalories=1000;
      let HIGHCARBCaloriesTexPercent:any = ((<HTMLInputElement>document.getElementById('HIGHCARBcal')).value);
      this.statCalories= (this.statCalories*HIGHCARBCaloriesTexPercent/100)*1;

      let HIGHCARBFatTexPercent:any = ((<HTMLInputElement>document.getElementById('HIGHCARBfat')).value);
      this.HIGHCARBFat= ((this.statCalories*HIGHCARBFatTexPercent/100)/9).toFixed(2);

      let HIGHCARBProteinTexPercent:any = ((<HTMLInputElement>document.getElementById('HIGHCARBprotein')).value);
      this.HIGHCARBProtein= ((this.statCalories*HIGHCARBProteinTexPercent/100)/4).toFixed(2);

      let HIGHCARBCarbsTexPercent:any = ((<HTMLInputElement>document.getElementById('HIGHCARBcarbs')).value);
      this.HIGHCARBCarbs= ((this.statCalories*HIGHCARBCarbsTexPercent/100)/4).toFixed(2);

    }
    
  
  }

  CaloriesRangeChange(Calorie_Range){
    debugger
    this.statCalories=Calorie_Range;
    this.DietTypeChange(this.DietType);

  }
  dietPlanStoreData={};
  ngOnInit() {
    // this.selectedFood.push(this.item);
  //  this.dietPlanStoreData = JSON.parse(localStorage.getItem('dietplan'));
   //this.dietPlan=[];
  // this.getDietPlanPreview()

  
    $("#Item_calculations").hide();

    $(document).ready(function () {

      $('ul.tabs li').click(function () {
        var tab_id = $(this).attr('data-tab');

        $('ul.tabs li').removeClass('current');
        $('.tab-content').removeClass('current');

        $(this).addClass('current');
        $("#" + tab_id).addClass('current');
      });



      $('ul.tabs1 li').click(function () {
        var tab_id = $(this).attr('data-tab');

        $('ul.tabs1 li').removeClass('current1');
        $('.tab-content1').removeClass('current1');

        $(this).addClass('current1');
        $("#" + tab_id).addClass('current1');
      });

      $('ul.tabs2 li').click(function () {
        var tab_id = $(this).attr('data-tab');

        $('ul.tabs2 li').removeClass('current2');
        $('.tab-content2').removeClass('current2');

        $(this).addClass('current2');
        $("#" + tab_id).addClass('current2');
      });

      $("#meal-desc1").hide();
      $("#meal-desc2").hide();

      $("#meal1").click(function () {
        $("#meal-desc1").show();
      });

      $("#meal2").click(function () {
        $("#meal-desc2").show();
      });

    });

    this.getDietFoodItem();


  }

}
