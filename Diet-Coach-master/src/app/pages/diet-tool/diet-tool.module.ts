import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DietToolComponent} from './diet-tool.component';
import { FormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {ModalModule} from "ngx-modal";
@NgModule({
  imports: [
    CommonModule,
    ModalModule,
    Ng2SearchPipeModule,
    FormsModule
  ],
  declarations: [DietToolComponent]
})
export class DietToolModule { }
