import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DietToolComponent } from './diet-tool.component';

describe('DietToolComponent', () => {
  let component: DietToolComponent;
  let fixture: ComponentFixture<DietToolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DietToolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DietToolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
